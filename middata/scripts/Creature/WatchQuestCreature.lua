local t = {}

t.events = {
    ObjectHookEvent.OnUnitDead,
}

function main(pCreature, questGuid, playerGuid)
    t.obj, t.questGuid, t.playerGuid = pCreature, questGuid, playerGuid
    pCreature:AttachObjectHookInfo(t)
end

function t.OnUnitDead(pKiller)
    local pPlayer = t.obj:GetMapInstance():GetAvailablePlayer(t.playerGuid)
    if pPlayer then
        pPlayer:RemoveQuestCreature(t.questGuid, t.obj)
    end
end