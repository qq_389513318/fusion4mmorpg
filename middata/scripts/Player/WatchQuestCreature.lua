local t = {}

t.events = {
    ObjectHookEvent.OnPlayerChangeQuestStatus,
}

function main(pPlayer)
    t.obj = pPlayer
    return pPlayer:AttachObjectHookInfo(t)
end

function t.OnPlayerChangeQuestStatus(pQuestLog, type)
    if type == QuestWhenType.Failed ||
       type == QuestWhenType.Cancel ||
       type == QuestWhenType.Submit then
        t.obj:StopWatchQuestCreature(pQuestLog:GetQuestGuid())
    end
end