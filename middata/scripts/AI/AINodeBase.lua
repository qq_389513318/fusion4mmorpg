function AIRunningNode(bb)
    local node = AIScriptableNode(bb)
    node:SetKernelScriptable(function(obj)
        return AINodeStatus.Running
    end)
    return node
end

function AIFinishedNode(bb)
    local node = AIScriptableNode(bb)
    node:SetKernelScriptable(function(obj)
        return AINodeStatus.Finished
    end)
    return node
end

function AIMoveToPriorPosition(bb)
    local node = AIScriptableNode(bb)
    node:SetKernelScriptable(function(obj)
        obj:MoveToPriorPosition()
        return AINodeStatus.Running
    end)
    return node
end

function AIResetToIdle(bb)
    local node = AIScriptableNode(bb)
    node:SetKernelScriptable(function(obj)
        obj:ResetToIdle()
        return AINodeStatus.Finished
    end)
    return node
end

function AIUpdateIdlePatrol(bb)
    local node = AIScriptableNode(bb)
    node:SetKernelScriptable(function(obj)
        obj:UpdateIdlePatrol()
        return AINodeStatus.Running
    end)
    return node
end