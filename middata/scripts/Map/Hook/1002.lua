function main(pMapInstance, pHook)
    pMapInstance:ApplyNavPhysicsFlags(-1, true)
    pHook:WatchShutdownMapInstance(5*1000, 5*60*1000)
end