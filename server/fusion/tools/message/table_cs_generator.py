import filecmp
import os

import table_ast
import table_lexer
import table_parser

class CsMessageHandler:
    def __init__(self, outdir, handlerName, msgPrefixs, msgTags, msgMax):
        self.outdir, self.handlerName = outdir, handlerName
        self.msgPrefixs, self.msgTags = msgPrefixs, msgTags
        self.msgMax = msgMax

def to_cs_files(indir, outdir, msgHandlers):
    def check_replace_file(filename):
        if not os.path.exists(filename[:-1]) or \
           not filecmp.cmp(filename, filename[:-1], False):
            os.replace(filename, filename[:-1])
        else:
            os.remove(filename)

    def parse_ast(infile):
        lexer = table_lexer.TableLexer()
        lexer.parse(infile)
        parser = table_parser.TableParser()
        return parser.parse(lexer)

    def parse_blanks(infile):
        blanks = []
        with open(infile, 'r', encoding='utf8') as fo:
            for i, line in enumerate(fo.readlines()):
                if line.isspace():
                    blanks.append(i + 1)
        return blanks

    def generate_cs_file(indir, outdir, filename):
        filepart = os.path.splitext(filename)[0]
        outpathpart = os.path.join(outdir, filepart)
        infile = os.path.abspath(os.path.join(indir, filename))
        print('cs files %s ...' % infile)
        root, blanks = parse_ast(infile), parse_blanks(infile)
        outfile, prefix = outpathpart+'.cs~', ''
        GeneratorCS(blanks).Generate(root, outfile, prefix)
        check_replace_file(outfile)
        return filepart, root

    def generate_message_handler(rootFileList, msgHandler):
        print('cs message handler %s ...' % msgHandler.handlerName)
        if not os.path.exists(msgHandler.outdir):
            os.mkdir(msgHandler.outdir)
        outfile = os.path.join(msgHandler.outdir, msgHandler.handlerName+'.cs~')
        prefix = 'using System;\n\n'
        GeneratorMHCS().Generate(outfile, rootFileList, msgHandler, prefix)
        check_replace_file(outfile)

    if os.path.exists(indir):
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        rootFileList = {}
        for filename in os.listdir(indir):
            name, root = generate_cs_file(indir, outdir, filename)
            rootFileList[name] = root
        for msgHandler in msgHandlers:
            generate_message_handler(rootFileList, msgHandler)

class GeneratorCS:
    def __init__(self, blanks):
        self.blanks, self.i = blanks, 0
        self.lineno = 0

    def Generate(self, root, outfile, prefix = ''):
        with open(outfile, 'w') as self.fo:
            self.fo.write(prefix)
            for entity in root.externalDeclarations:
                self.__generateentity(entity, 0)

    def __generateentity(self, entity, level):
        if isinstance(entity, table_ast.EnumDefinition):
            return self.__generateenumblock(entity, level)

        if isinstance(entity, table_ast.EnumDeclaration):
            return self.__generateenummember(entity, level)

        if isinstance(entity, table_ast.Preprocessor):
            return self.__writeline2file(level,
                '//' + entity.text.value.rstrip(), entity.text.lineno)

        if isinstance(entity, table_ast.Comment):
            return self.__writeline2file(level,
                entity.text.value.rstrip(), entity.text.lineno,
                canInline = True, strSpacer = '  ')

    def __generateenumblock(self, entity, level):
        self.__writeline2file(level, 'public enum ' + entity.name.value,
            entity.name.lineno)
        self.__generateblockmember(entity, level)

    def __generateblockmember(self, entity, level):
        self.__writeline2file(level, '{')
        for member in entity.declarationList.declarationList:
            self.__generateentity(member, level + 1)
        self.__writeline2file(level, '};')

    def __generateenummember(self, member, level):
        self.__writeline2file(level, member.memName.value +
            self.__enumValue2formatStr(member.memValue) + ',',
            member.memName.lineno, canInline = True, strSpacer = ' ')

    def __writeline2file(self, level, linedata, lineno = -1, **kwargs):
        while len(self.blanks) > self.i and self.blanks[self.i] < lineno:
            self.fo.write('\n')
            self.i += 1
        if kwargs and kwargs['canInline'] and self.lineno == lineno:
            self.fo.seek(self.fo.tell() - len(os.linesep))
            self.fo.write(kwargs['strSpacer'])
        elif level != 0:
            self.fo.write('\t' * level)
        self.fo.write(linedata)
        self.fo.write('\n')
        if lineno != -1:
            self.lineno = lineno

    @classmethod
    def __enumValue2formatStr(cls, node):
        return (' = ' + (cls.__to_cs_ns(node) if
            isinstance(node, table_ast.NsIdList) else node)) if node else ''

    @staticmethod
    def __to_cs_ns(node):
        return '.'.join(ns.value for ns in node.idList)

class GeneratorMHCS:
    def Generate(self, outfile, rootFileList, msgHandler, prefix = ''):
        with open(outfile, 'w') as self.fo:
            msgs = self.__filter_message(
                rootFileList, msgHandler.msgPrefixs, msgHandler.msgTags)
            self.fo.write(prefix)
            self.fo.write('public partial class %s\n' % msgHandler.handlerName)
            self.fo.write('{\n')
            self.fo.write('\tprivate delegate void HandleFunc(NetPacket);\n')
            self.fo.write('\tprivate delegate void RPCHandleFunc(NetPacket, RPCReqMetaInfo);\n')
            self.fo.write('\n')
            self.fo.write('\tprivate HandleFunc[] handles_ = new HandleFunc[%s];\n' % msgHandler.msgMax)
            self.fo.write('\tprivate RPCHandleFunc[] rpc_handles_ = new RPCHandleFunc[%s];\n' % msgHandler.msgMax)
            self.fo.write('\n')
            self.fo.write('\tpublic void InitPacketHandlerTable()\n')
            self.fo.write('\t{\n')
            for msg in msgs:
                self.__write_message_handler(msg, msgHandler)
            self.fo.write('\t}\n')
            self.fo.write('}\n')

    def __write_message_handler(self, msg, msgHandler):
        if not msg.rpc:
            self.fo.write('\t\thandlers_[(int)%s.%s] = new HandleFunc(%s);\n' %
                (msg.parts[0], msg.parts[1], self.__beautify_message_name(msg.parts[1])))
        else:
            self.fo.write('\t\trpc_handles_[(int)%s.%s] = new RPCHandleFunc(%s);\n' %
                (msg.parts[0], msg.parts[1], self.__beautify_message_name(msg.parts[1])))

    class MsgInfo:
        def __init__(self, rpc, *parts):
            assert len(parts) == 2, 'msg part number must equal 2.'
            self.rpc, self.parts = rpc, parts

    @staticmethod
    def __beautify_message_name(msgName):
        parts = [part.title() for part in msgName.split('_')]
        return 'Handle' + ''.join(parts[1 if len(parts) > 1 else 0 :])

    @classmethod
    def __filter_message(cls, rootFileList, msgPrefixs, msgTags):
        msgs = []
        for name, root in rootFileList.items():
            for entity in root.externalDeclarations:
                if not isinstance(entity, table_ast.EnumDefinition):
                    continue
                for i, member in enumerate(entity.declarationList.declarationList):
                    if not isinstance(member, table_ast.EnumDeclaration):
                        continue
                    tags = cls.__get_message_tags(entity.
                        declarationList.declarationList[i+1:], member.memName.lineno)
                    if not cls.__check_message_available(
                        member.memName.value, tags, msgPrefixs, msgTags):
                        continue
                    msgs.append(cls.MsgInfo(tags and 'rpc' in tags or False,
                        entity.name.value, member.memName.value))
        return msgs

    @staticmethod
    def __get_message_tags(declarationList, lineno):
        for member in declarationList:
            if isinstance(member, table_ast.EnumDeclaration):
                if member.memName.lineno <= lineno:
                    continue
            if isinstance(member, table_ast.Comment):
                if member.text.lineno == lineno:
                    try:
                        return member.text.tags
                    except AttributeError as e:
                        pass
            return

    @staticmethod
    def __check_message_available(memName, tags, msgPrefixs, msgTags):
        if tags and (len(tags) > 1 or tags[0] != 'rpc'):
            return 'ingore' not in tags and \
                next((True for msgTag in msgTags if msgTag in tags), False)
        else:
            return next((True for msgPrefix in \
                msgPrefixs if memName.startswith(msgPrefix + '_')), False)
