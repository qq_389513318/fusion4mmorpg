#include "System.h"

time_t System::unix_time_;
uint64 System::sys_time_;
uint64 System::start_time_;
std::atomic<std::shared_ptr<System::AtomicDateTime>> System::date_time_;
#if defined(_WIN32)
    LARGE_INTEGER System::performance_frequency_;
#endif
std::default_random_engine System::random_engine_;

void System::Init()
{
#if defined(_WIN32)
    QueryPerformanceFrequency(&performance_frequency_);
#endif
    random_engine_.seed(std::random_device()());
    start_time_ = GetRealSysTime();
}

void System::Update()
{
    auto date_time = std::make_shared<AtomicDateTime>();
    ::time(&date_time->time_);
    ::localtime_r(&date_time->time_, &date_time->date_);
    date_time_.store(date_time);
    unix_time_ = date_time->time_;
    sys_time_ = GetRealSysTime();
}

s64 System::Rand(s64 lower, s64 upper, bool closed)
{
    if (lower > upper) std::swap(lower, upper);
    if (lower == upper || lower == (upper -= (closed ? 0 : 1))) return lower;
    return std::uniform_int_distribution<s64>(lower, upper)(random_engine_);
}

s64 System::RandS64(s64 lower, s64 upper, bool closed)
{
    return Rand(lower, upper, closed);
}

int System::Rand(int lower, int upper, bool closed)
{
    if (lower > upper) std::swap(lower, upper);
    if (lower == upper || lower == (upper -= (closed ? 0 : 1))) return lower;
    return std::uniform_int_distribution<int>(lower, upper)(random_engine_);
}

int System::Randi(int lower, int upper, bool closed)
{
    return Rand(lower, upper, closed);
}

float System::Rand(float lower, float upper)
{
    if (lower == upper) return lower;
    if (lower > upper) std::swap(lower, upper);
    return std::uniform_real_distribution<float>(lower, upper)(random_engine_);
}

float System::Randf(float lower, float upper)
{
    return Rand(lower, upper);
}

static inline int PassDayTime(const struct tm &tm) {
    return tm.tm_hour * 60*60 + tm.tm_min * 60 + tm.tm_sec;
}
static inline int PassWeekTime(const struct tm &tm) {
    return (tm.tm_wday != 0 ? tm.tm_wday - 1 : 6) * 60*60*24 + PassDayTime(tm);
}
static inline int PassMonthTime(const struct tm &tm) {
    return (tm.tm_mday - 1) * 60*60*24 + PassDayTime(tm);
}

time_t System::GetDayUnixTime(const AtomicDateTime &t)
{
    return t.time_ - PassDayTime(t.date_);
}

time_t System::GetWeekUnixTime(const AtomicDateTime &t)
{
    return t.time_ - PassWeekTime(t.date_);
}

time_t System::GetMonthUnixTime(const AtomicDateTime &t)
{
    return t.time_ - PassMonthTime(t.date_);
}

time_t System::GetDayUnixTime()
{
    return GetDayUnixTime(*date_time_.load());
}

time_t System::GetWeekUnixTime()
{
    return GetWeekUnixTime(*date_time_.load());
}

time_t System::GetMonthUnixTime()
{
    return GetMonthUnixTime(*date_time_.load());
}

struct tm System::GetDateTime()
{
    return date_time_.load()->date_;
}

time_t System::GetDayUnixTime(time_t t)
{
    auto day = GetDayUnixTime();
    if (t >= day) {
        return t - (t - day) % (60*60*24);
    } else {
        auto dt = (day - t) % (60*60*24);
        return t + (dt != 0 ? dt - (60*60*24) : 0);
    }
}

time_t System::GetWeekUnixTime(time_t t)
{
    auto week = GetWeekUnixTime();
    if (t >= week) {
        return t - (t - week) % (60*60*24*7);
    } else {
        auto dt = (week - t) % (60*60*24*7);
        return t + (dt != 0 ? dt - (60*60*24*7) : 0);
    }
}

time_t System::GetMonthUnixTime(time_t t)
{
    struct tm tm;
    ::localtime_r(&t, &tm);
    return t - PassMonthTime(tm);
}

uint64 System::GetRealSysTime()
{
#if defined(_WIN32)
    LARGE_INTEGER performance_counter;
    QueryPerformanceCounter(&performance_counter);
    return 1000 * performance_counter.QuadPart / performance_frequency_.QuadPart;
#else
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    return tp.tv_sec * 1000 + tp.tv_nsec / 1000000;
#endif
}
