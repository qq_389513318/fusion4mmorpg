#pragma once

#include <functional>
#include "lua/LuaRef.h"

class AIActor;

class AIBlackboard : public lua::binder, public noncopyable
{
public:
    AIBlackboard();
    ~AIBlackboard();

    template <typename T>
    void SetActor(lua_State *L, T *actor) {
        lua::push<T*>::invoke(L, actor);
        ai_actor_cache_ = LuaRef(L, true);
        ai_actor_ = actor;
    }

    AIActor *actor() const { return ai_actor_; }
    const LuaRef &actor_cache() const { return ai_actor_cache_; }

    std::function<void(lua_State *L, int index)> RefTreeNode;

private:
    AIActor *ai_actor_;
    LuaRef ai_actor_cache_;
};
