#include "RBTreeRank.h"

RBTreeRankBase::RBTreeRankBase()
: root_(&nil_)
, nil_{RBTreeRankBase::BLACK}
{
}

RBTreeRankBase::~RBTreeRankBase()
{
}

void RBTreeRankBase::LeftRotate(NodeBase *x)
{
    NodeBase *y = x->right;
    x->right = y->left;
    if (y->left != &nil_) {
        y->left->p = x;
    }
    y->p = x->p;
    if (x->p == &nil_) {
        root_ = y;
    } else if (x == x->p->left) {
        x->p->left = y;
    } else {
        x->p->right = y;
    }
    y->left = x;
    x->p = y;
    y->n += x->n + 1;
}

void RBTreeRankBase::RightRotate(NodeBase *x)
{
    NodeBase *y = x->left;
    x->left = y->right;
    if (y->right != &nil_) {
        y->right->p = x;
    }
    y->p = x->p;
    if (x->p == &nil_) {
        root_ = y;
    } else if (x == x->p->right) {
        x->p->right = y;
    } else {
        x->p->left = y;
    }
    y->right = x;
    x->p = y;
    x->n -= y->n + 1;
}

void RBTreeRankBase::RBTransplant(NodeBase *u, NodeBase *v)
{
    if (u->p == &nil_) {
        root_ = v;
    } else if (u == u->p->left) {
        u->p->left = v;
    } else {
        u->p->right = v;
    }
    v->p = u->p;
}

void RBTreeRankBase::RBInsertFixup(NodeBase *z)
{
    while (z->p->color == RED) {
        if (z->p == z->p->p->left) {
            auto y = z->p->p->right;
            if (y->color == RED) {
                z->p->color = BLACK;
                y->color = BLACK;
                z->p->p->color = RED;
                z = z->p->p;
            } else {
                if (z == z->p->right) {
                    z = z->p;
                    LeftRotate(z);
                }
                z->p->color = BLACK;
                z->p->p->color = RED;
                RightRotate(z->p->p);
            }
        } else {
            auto y = z->p->p->left;
            if (y->color == RED) {
                z->p->color = BLACK;
                y->color = BLACK;
                z->p->p->color = RED;
                z = z->p->p;
            } else {
                if (z == z->p->left) {
                    z = z->p;
                    RightRotate(z);
                }
                z->p->color = BLACK;
                z->p->p->color = RED;
                LeftRotate(z->p->p);
            }
        }
    }
    root_->color = BLACK;
}

void RBTreeRankBase::RBDelete(NodeBase *z)
{
    NodeBase *x = &nil_;
    NodeBase *y = z;
    Color y_original_color = y->color;
    if (z->left == &nil_) {
        x = z->right;
        TreeNFixup(z, -1);
        RBTransplant(z, z->right);
    } else if (z->right == &nil_) {
        x = z->left;
        TreeNFixup(z, -1);
        RBTransplant(z, z->left);
    } else {
        y = TreeMinimum(z->right);
        y_original_color = y->color;
        x = y->right;
        TreeNFixup(y, -1);
        if (y->p == z) {
            x->p = y;
        } else {
            RBTransplant(y, y->right);
            y->right = z->right;
            y->right->p = y;
        }
        RBTransplant(z, y);
        y->left = z->left;
        y->left->p = y;
        y->color = z->color;
        y->n = z->n;
    }
    if (y_original_color == BLACK) {
        RBDeleteFixup(x);
    }
}

void RBTreeRankBase::RBDeleteFixup(NodeBase *x)
{
    while (x != root_ && x->color == BLACK) {
        if (x == x->p->left) {
            auto w = x->p->right;
            if (w->color == RED) {
                w->color = BLACK;
                x->p->color = RED;
                LeftRotate(x->p);
                w = x->p->right;
            }
            if (w->left->color == BLACK && w->right->color == BLACK) {
                w->color = RED;
                x = x->p;
            } else {
                if (w->right->color == BLACK) {
                    w->left->color = BLACK;
                    w->color = RED;
                    RightRotate(w);
                    w = x->p->right;
                }
                w->color = x->p->color;
                x->p->color = BLACK;
                w->right->color = BLACK;
                LeftRotate(x->p);
                x = root_;
            }
        } else {
            auto w = x->p->left;
            if (w->color == RED) {
                w->color = BLACK;
                x->p->color = RED;
                RightRotate(x->p);
                w = x->p->left;
            }
            if (w->right->color == BLACK && w->left->color == BLACK) {
                w->color = RED;
                x = x->p;
            } else {
                if (w->left->color == BLACK) {
                    w->right->color = BLACK;
                    w->color = RED;
                    LeftRotate(w);
                    w = x->p->left;
                }
                w->color = x->p->color;
                x->p->color = BLACK;
                w->left->color = BLACK;
                RightRotate(x->p);
                x = root_;
            }
        }
    }
    x->color = BLACK;
}

RBTreeRankBase::NodeBase *RBTreeRankBase::TreeMinimum(NodeBase *x) const
{
    while (x->left != &nil_) {
        x = x->left;
    }
    return x;
}

RBTreeRankBase::NodeBase *RBTreeRankBase::TreeMaximum(NodeBase *x) const
{
    while (x->right != &nil_) {
        x = x->right;
    }
    return x;
}

RBTreeRankBase::NodeBase *RBTreeRankBase::TreeSuccessor(NodeBase *x) const
{
    if (x->right != &nil_) {
        x = x->right;
        while (x->left != &nil_) {
            x = x->left;
        }
        return x;
    } else {
        auto y = x->p;
        while (y != &nil_ && x == y->right) {
            x = y;
            y = y->p;
        }
        return y;
    }
}

RBTreeRankBase::NodeBase *RBTreeRankBase::TreePredecessor(NodeBase *x) const
{
    if (x->left != &nil_) {
        x = x->left;
        while (x->right != &nil_) {
            x = x->right;
        }
        return x;
    } else {
        auto y = x->p;
        while (y != &nil_ && x == y->left) {
            x = y;
            y = y->p;
        }
        return y;
    }
}

size_t RBTreeRankBase::TreeRank(NodeBase *x) const
{
    auto n = x->n;
    auto y = x->p;
    while (y != &nil_) {
        if (y->right == x) {
            n += y->n + 1;
        }
        x = y;
        y = y->p;
    }
    return n;
}

RBTreeRankBase::NodeBase *RBTreeRankBase::TreeAdvance(size_t i) const
{
    size_t n = 0;
    NodeBase *x = root_;
    while (x != &nil_) {
        auto m = x->n + n;
        if (m < i) {
            n = m + 1;
            x = x->right;
        } else if (m > i) {
            x = x->left;
        } else {
            break;
        }
    }
    return x;
}

void RBTreeRankBase::TreeNFixup(NodeBase *x, ssize_t n) const
{
    auto y = x->p;
    while (y != &nil_) {
        if (y->left == x) {
            y->n += n;
        }
        x = y;
        y = y->p;
    }
}
