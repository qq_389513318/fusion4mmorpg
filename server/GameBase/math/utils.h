#pragma once

inline float Calc2DIncludedAngle(float angle1, float angle2) {
	float diffAngle = fmodf(angle1 - angle2, 360.f);
	return diffAngle > PI ? (diffAngle - PI*2) :
		(diffAngle < -PI ? diffAngle + PI*2 : diffAngle);
}
inline float Calc2DIncludedAngle(const vector3f& dir1, const vector3f& dir2) {
	return Calc2DIncludedAngle(dir1.Calc2DAngle(), dir2.Calc2DAngle());
}

inline float dotProduct(const vector3f& arg1, const vector3f& arg2) {
	return arg1.x*arg2.x + arg1.y*arg2.y + arg1.z*arg2.z;
}
inline vector3f crossProduct(const vector3f& arg1, const vector3f& arg2) {
	return {arg1.y*arg2.z - arg1.z*arg2.y,
			arg1.z*arg2.x - arg1.x*arg2.z,
			arg1.x*arg2.y - arg1.y*arg2.x};
}

inline vector3f RandomRotateByRadius(const vector3f& pos, float radius) {
	vector3f dir{0,0,1};
	dir.rotateXZBy(System::Rand(-PI, PI));
	return pos + (dir *= radius);
}
inline vector3f CalcRotateByRadius(const vector3f& pos, float radius, float angle) {
	vector3f dir{0,0,1};
	dir.rotateXZBy(angle);
	return pos + (dir *= radius);
}

inline vector3f RandomPositionByRadius(const vector3f& pos, float minRadius, float maxRadius) {
	return RandomRotateByRadius(pos, sqrtf(System::Rand(minRadius*minRadius, maxRadius*maxRadius)));
}
inline vector3f RandomPositionByRadius(const vector3f& pos, float radius) {
	return RandomRotateByRadius(pos, sqrtf(System::Rand(.0f, radius*radius)));
}
inline vector3f RandomPositionByRange(const vector3f& pos, float range) {
	return {pos.x + System::Rand(-range, range),
			pos.y,
			pos.z + System::Rand(-range, range)};
}
