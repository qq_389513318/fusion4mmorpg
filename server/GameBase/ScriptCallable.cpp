#include "preHeader.h"
#include "ScriptCallable.h"

int CastCallable(lua_State* L)
{
	lua::push<std::function<void()>&&>::invoke(L,
		[funPtr = std::make_shared<LuaRef>(L, 1)]() {
		LuaFunc(*funPtr).Call<void>();
	});
	return 1;
}

int CastCallableWithDtor(lua_State* L)
{
	class Actor {
	public:
		Actor(lua_State* L) {
			callable_ = LuaRef(L, 1);
			destructor_ = LuaRef(L, 2);
		}
		~Actor() {
			LuaFunc(destructor_).Call<void>();
		}
		void Invoke() {
			LuaFunc(callable_).Call<void>();
		}
	private:
		LuaRef callable_, destructor_;
	};
	lua::push<std::function<void()>&&>::invoke(L,
		[actPtr = std::make_shared<Actor>(L)]() {
		actPtr->Invoke();
	});
	return 1;
}
