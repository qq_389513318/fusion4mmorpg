#pragma once

enum InstanceFlag
{
	InstanceAutoUidFlag = 1 << 0,
};

enum class ArenaFightStage
{
	eUnInit,
	eStarting,
	ePreparing,
	eFighting,
	eFinished,
	eClosed,
};

struct AuraPrototype
{
	uint32 lifeTime;
	uint32 auraTargetType;
	uint32 auraDelayEffective;
	float auraEffectiveRadius;
	uint32 effectSpellID;
	uint32 effectSpellLevel;
	int32 effectIndexes;
	bool isFollowOwner;
	bool isLeaveContinue;
	bool isClientVisible;
};
