#pragma once

#define QUEST_INTERACTIVE_DIST_MAX (10.f)

enum QUEST_STATUS
{
	QMGR_QUEST_NOT_AVAILABLE,
	QMGR_QUEST_AVAILABLELOW_LEVEL,
	QMGR_QUEST_AVAILABLE,
	QMGR_QUEST_NOT_FINISHED,
	QMGR_QUEST_FINISHED,
};

struct QuestNavData
{
	uint32 mapId;
	uint32 mapType;
	int32 mapLine;
	vector3f pos;
	enum Flag {
		PREVENT_SYNC_CLIENT = 1 << 0,
		PREVENT_SYNC_SERVER = 1 << 1,
	};
};

struct QuestNavObjInst
{
	uint32 objType;
	uint32 objID;
	uint32 objArgType;
	uint32 objArgs[1];
	enum ArgType {
		None,
		GELevel,
	};
};

struct inst_quest_prop
{
	inst_quest_prop();

	enum Flag {
		IsFinished,
		IsFailed,
	};
	struct Condition {
		int64 progress;
		int64 userData;
	};

	uint32 questGuid;
	uint32 questTypeID;
	uint32 questFlags;
	int64 questExpireTime;
	std::vector<Condition> questConditions;

	void Save(INetStream& pck) const;
	void Load(INetStream& pck);

	void Save(TextPacker& packer) const;
	void Load(TextUnpacker& unpacker);
};

bool IsCareQuestStatus(QUEST_STATUS questStatus);
