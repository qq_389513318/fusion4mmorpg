#pragma once

#include "Singleton.h"
#include "rpc/RPCSession.h"

class DBPSessionHandler;

class DBPSession : public RPCSession
{
public:
	DBPSession();
	virtual ~DBPSession();

	void Init(const std::string& host, const std::string& port);

	void CheckConnection();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void DeleteObject();

	static DBPSession* New(const std::string& host, const std::string& port);

private:
	friend DBPSessionHandler;
	std::string m_host;
	std::string m_port;
};

class DBPSessionSingleton :
	public DBPSession, public Singleton<DBPSessionSingleton> {};
#define sDBPSession (*DBPSessionSingleton::instance())
RPCError sDBPSessionUpdate4RPCBlockInvoke();
