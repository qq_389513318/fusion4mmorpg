#include "preHeader.h"
#include "VarDefines.h"

const ObjGUID ObjGUID_NULL = { 0 };
const InstGUID InstGUID_NULL = { 0 };

const int objTypeFlags[] = {
	TYPE_OBJECT_FLAG,
	TYPE_LOCATABLEOBJECT_FLAG,
	TYPE_STATICOBJECT_FLAG,
	TYPE_AURAOBJECT_FLAG,
	TYPE_UNIT_FLAG,
	TYPE_CREATURE_FLAG,
	TYPE_PLAYER_FLAG,
	TYPE_AUTOPLAYER_FLAG,
};
STATIC_ASSERT(ARRAY_SIZE(objTypeFlags)==TYPE_MAX);

const int objValuesCount[] = {
	OBJECT_END,
	LOBJ_END,
	SOBJ_END,
	AOBJ_END,
	UNIT_END,
	CREATURE_END,
	PLAYER_END,
	APC_END,
};
STATIC_ASSERT(ARRAY_SIZE(objValuesCount)==TYPE_MAX);

const int obj64ValuesCount[] = {
	OBJECT64_END,
	LOBJ64_END,
	SOBJ64_END,
	AOBJ64_END,
	UNIT64_END,
	CREATURE64_END,
	PLAYER64_END,
	APC64_END,
};
STATIC_ASSERT(ARRAY_SIZE(obj64ValuesCount)==TYPE_MAX);

const vector3f vector3f_INVALID{FLT_MAX,FLT_MAX,FLT_MAX};
const vector3f vector3f_NULL;
const vector3f1f vector3f1f_NULL;
const vector3f3f vector3f3f_NULL;
