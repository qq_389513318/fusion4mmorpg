-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2020-07-08 12:03:13
-- 服务器版本： 8.0.20
-- PHP 版本： 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `mmorpg_char`
--

-- --------------------------------------------------------

--
-- 表的结构 `guild`
--

CREATE TABLE `guild` (
  `Id` int UNSIGNED NOT NULL,
  `gsId` int UNSIGNED NOT NULL,
  `name` varchar(256) NOT NULL,
  `createTime` bigint NOT NULL,
  `level` int UNSIGNED NOT NULL,
  `levelTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `guild_apply`
--

CREATE TABLE `guild_apply` (
  `playerId` int UNSIGNED NOT NULL,
  `guildId` int UNSIGNED NOT NULL,
  `applyTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `guild_member`
--

CREATE TABLE `guild_member` (
  `playerId` int UNSIGNED NOT NULL,
  `guildId` int UNSIGNED NOT NULL,
  `guildTitle` tinyint NOT NULL,
  `joinTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_configure`
--

CREATE TABLE `inst_configure` (
  `cfgID` int UNSIGNED NOT NULL,
  `cfgValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_mail`
--

CREATE TABLE `inst_mail` (
  `mailID` int UNSIGNED NOT NULL,
  `mailType` int UNSIGNED NOT NULL,
  `mailFlags` int UNSIGNED NOT NULL,
  `mailSender` int UNSIGNED NOT NULL,
  `mailSenderName` varchar(128) NOT NULL,
  `mailReceiver` int UNSIGNED NOT NULL,
  `mailDeliverTime` bigint NOT NULL,
  `mailExpireTime` bigint NOT NULL,
  `mailSubject` varchar(256) NOT NULL,
  `mailBody` text NOT NULL,
  `mailCheques` json NOT NULL,
  `mailItems` json NOT NULL,
  `isGetAttachment` tinyint(1) NOT NULL,
  `isViewDetail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_operating_activities`
--

CREATE TABLE `inst_operating_activities` (
  `playerId` int UNSIGNED NOT NULL,
  `actType` int UNSIGNED NOT NULL,
  `actTimes` int UNSIGNED NOT NULL,
  `saveData` json NOT NULL,
  `userData` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_player_char`
--

CREATE TABLE `inst_player_char` (
  `ipcInstID` int UNSIGNED NOT NULL,
  `ipcAcctID` int UNSIGNED NOT NULL,
  `ipcNickName` varchar(128) NOT NULL,
  `ipcFlags` json NOT NULL,
  `ipcAppearance` json NOT NULL,
  `ipcCareer` tinyint UNSIGNED NOT NULL,
  `ipcGender` tinyint UNSIGNED NOT NULL,
  `ipcMapType` smallint UNSIGNED NOT NULL,
  `ipcMapID` smallint UNSIGNED NOT NULL,
  `ipcPosX` float NOT NULL,
  `ipcPosY` float NOT NULL,
  `ipcPosZ` float NOT NULL,
  `ipcPosO` float NOT NULL,
  `ipcVipLevel` int UNSIGNED NOT NULL,
  `ipcLevel` int UNSIGNED NOT NULL,
  `ipcExp` bigint NOT NULL,
  `ipcCurHP` bigint NOT NULL,
  `ipcCurMP` bigint NOT NULL,
  `ipcMoneyGold` bigint NOT NULL,
  `ipcMoneyDiamond` bigint NOT NULL,
  `ipcServerID` int UNSIGNED NOT NULL,
  `ipcCreateTime` bigint NOT NULL,
  `ipcLastLoginTime` bigint NOT NULL,
  `ipcLastOnlineTime` bigint NOT NULL,
  `ipcDeleteTime` bigint NOT NULL,
  `ipcVipStatus` mediumtext NOT NULL,
  `ipcQuestsDone` mediumtext NOT NULL,
  `ipcQuests` mediumtext NOT NULL,
  `ipcEffectItems` mediumtext NOT NULL,
  `ipcAllOtherItems` mediumtext NOT NULL,
  `ipcStorageStatus` mediumtext NOT NULL,
  `ipcShopStatus` mediumtext NOT NULL,
  `ipcSpells` mediumtext NOT NULL,
  `ipcBuffs` mediumtext NOT NULL,
  `ipcCooldowns` mediumtext NOT NULL,
  `ipcJsonValue` json NOT NULL,
  `ipcF32Values` json NOT NULL,
  `ipcS32Values` json NOT NULL,
  `ipcS64Values` json NOT NULL,
  `ipcPropertyValue` json NOT NULL,
  `ipcRankValue` json NOT NULL,
  `ipcGsReadValue` json NOT NULL,
  `ipcGsWriteValue` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `social_friend`
--

CREATE TABLE `social_friend` (
  `characterId` int UNSIGNED NOT NULL,
  `friendId` int UNSIGNED NOT NULL,
  `createTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- 表的结构 `social_ignore`
--

CREATE TABLE `social_ignore` (
  `characterId` int UNSIGNED NOT NULL,
  `ignoreId` int UNSIGNED NOT NULL,
  `createTime` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `guild`
--
ALTER TABLE `guild`
  ADD PRIMARY KEY (`Id`);

--
-- 表的索引 `guild_apply`
--
ALTER TABLE `guild_apply`
  ADD UNIQUE KEY `playerId` (`playerId`,`guildId`);

--
-- 表的索引 `guild_member`
--
ALTER TABLE `guild_member`
  ADD PRIMARY KEY (`playerId`);

--
-- 表的索引 `inst_configure`
--
ALTER TABLE `inst_configure`
  ADD PRIMARY KEY (`cfgID`);

--
-- 表的索引 `inst_mail`
--
ALTER TABLE `inst_mail`
  ADD PRIMARY KEY (`mailID`),
  ADD UNIQUE KEY `mailExpireTime` (`mailExpireTime`),
  ADD KEY `mailReceiver` (`mailReceiver`,`mailDeliverTime`) USING BTREE;

--
-- 表的索引 `inst_operating_activities`
--
ALTER TABLE `inst_operating_activities`
  ADD PRIMARY KEY (`actType`,`actTimes`,`playerId`);

--
-- 表的索引 `inst_player_char`
--
ALTER TABLE `inst_player_char`
  ADD PRIMARY KEY (`ipcInstID`),
  ADD UNIQUE KEY `ipcNickName` (`ipcNickName`),
  ADD KEY `ipcAcctID` (`ipcAcctID`);

--
-- 表的索引 `social_friend`
--
ALTER TABLE `social_friend`
  ADD UNIQUE KEY `characterId` (`characterId`,`friendId`);

--
-- 表的索引 `social_ignore`
--
ALTER TABLE `social_ignore`
  ADD UNIQUE KEY `characterId` (`characterId`,`ignoreId`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `inst_mail`
--
ALTER TABLE `inst_mail`
  MODIFY `mailID` int UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
