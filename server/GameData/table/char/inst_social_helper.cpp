#include "jsontable/table_helper.h"
#include "inst_social.h"

template<> const char *GetTableName<social_friend>()
{
	return "social_friend";
}

template<> const char *GetTableKeyName<social_friend>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const social_friend &entity)
{
	return 0;
}

template<> void SetTableKeyValue(social_friend &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<social_friend>(size_t index)
{
	switch (index)
	{
		case 0: return "characterId";
		case 1: return "friendId";
		case 2: return "createTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<social_friend>(const char *name)
{
	if (strcmp(name, "characterId") == 0) return 0;
	if (strcmp(name, "friendId") == 0) return 1;
	if (strcmp(name, "createTime") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<social_friend>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const social_friend &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.characterId);
		case 1: return StringHelper::ToString(entity.friendId);
		case 2: return StringHelper::ToString(entity.createTime);
	}
	return "";
}

template<> void SetTableFieldValue(social_friend &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.characterId, value);
		case 1: return StringHelper::FromString(entity.friendId, value);
		case 2: return StringHelper::FromString(entity.createTime, value);
	}
}

template<> void LoadFromStream(social_friend &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.characterId, stream);
	StreamHelper::FromStream(entity.friendId, stream);
	StreamHelper::FromStream(entity.createTime, stream);
}

template<> void SaveToStream(const social_friend &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.characterId, stream);
	StreamHelper::ToStream(entity.friendId, stream);
	StreamHelper::ToStream(entity.createTime, stream);
}

template<> void LoadFromText(social_friend &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const social_friend &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(social_friend &entity, const rapidjson::Value &value)
{
	FromJson(entity.characterId, value, "characterId");
	FromJson(entity.friendId, value, "friendId");
	FromJson(entity.createTime, value, "createTime");
}

template<> void JsonHelper::BlockToJson(const social_friend &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.characterId, value, "characterId");
	ToJson(entity.friendId, value, "friendId");
	ToJson(entity.createTime, value, "createTime");
}

template<> const char *GetTableName<social_ignore>()
{
	return "social_ignore";
}

template<> const char *GetTableKeyName<social_ignore>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const social_ignore &entity)
{
	return 0;
}

template<> void SetTableKeyValue(social_ignore &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<social_ignore>(size_t index)
{
	switch (index)
	{
		case 0: return "characterId";
		case 1: return "ignoreId";
		case 2: return "createTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<social_ignore>(const char *name)
{
	if (strcmp(name, "characterId") == 0) return 0;
	if (strcmp(name, "ignoreId") == 0) return 1;
	if (strcmp(name, "createTime") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<social_ignore>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const social_ignore &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.characterId);
		case 1: return StringHelper::ToString(entity.ignoreId);
		case 2: return StringHelper::ToString(entity.createTime);
	}
	return "";
}

template<> void SetTableFieldValue(social_ignore &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.characterId, value);
		case 1: return StringHelper::FromString(entity.ignoreId, value);
		case 2: return StringHelper::FromString(entity.createTime, value);
	}
}

template<> void LoadFromStream(social_ignore &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.characterId, stream);
	StreamHelper::FromStream(entity.ignoreId, stream);
	StreamHelper::FromStream(entity.createTime, stream);
}

template<> void SaveToStream(const social_ignore &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.characterId, stream);
	StreamHelper::ToStream(entity.ignoreId, stream);
	StreamHelper::ToStream(entity.createTime, stream);
}

template<> void LoadFromText(social_ignore &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const social_ignore &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(social_ignore &entity, const rapidjson::Value &value)
{
	FromJson(entity.characterId, value, "characterId");
	FromJson(entity.ignoreId, value, "ignoreId");
	FromJson(entity.createTime, value, "createTime");
}

template<> void JsonHelper::BlockToJson(const social_ignore &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.characterId, value, "characterId");
	ToJson(entity.ignoreId, value, "ignoreId");
	ToJson(entity.createTime, value, "createTime");
}
