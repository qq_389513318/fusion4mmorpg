#pragma once

enum class ScriptType
{
	None = 0,
	AIGraphml = 1,
	CharSpawn,
	CharDead,
	CharPlay,
	SObjSpawn = 11,
	SObjPlay,
	QuestReq = 21,
	QuestInit,
	QuestReward,
	QuestEvent,
	CanCastSpell = 31,
	UseItem = 41,
};

struct Scriptable
{
	Scriptable();

	uint32 scriptId;
	uint32 scriptType;
	std::string scriptFile;
};
