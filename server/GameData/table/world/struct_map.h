#pragma once

struct MapInfo
{
	MapInfo();

	enum class Type {
		VoidSpace,
		WorldMap,
		Story,
		PlayBoss,
		GuildLeague,
		Count,
	};
	struct Flags {
		Flags();
		bool isParallelMap;
		bool isRecoveryHPDisable;
	};
	uint32 Id;
	uint32 type;
	Flags flags;
	std::string strName;
	std::string strSceneFile;
	float viewing_distance;
	uint32 load_value;
	uint32 pop_map_id;
	float pop_pos_x;
	float pop_pos_y;
	float pop_pos_z;
	float pop_o;
};

struct MapZone
{
	MapZone();

	uint32 Id;
	uint32 parentId;
	uint32 priority;
	uint32 map_id;
	uint32 map_type;
	float x1, y1, z1;
	float x2, y2, z2;
	uint32 pvp_flags;
};

struct MapGraveyard
{
	MapGraveyard();

	uint32 Id;
	std::string name;
	uint32 map_id;
	uint32 map_type;
	float x, y, z, o;
	uint32 trait;
};

struct TeleportPoint
{
	TeleportPoint();

	uint32 Id;
	std::string name;
	uint32 map_id;
	uint32 map_type;
	float x, y, z, o;
	uint32 trait;
};

struct WayPoint
{
	WayPoint();

	uint32 Id;
	uint32 first_wp_id;
	uint32 prev_wp_id;
	uint32 next_wp_id;
	uint32 map_id;
	float x, y, z;
	int32 keep_idle;
	uint32 emote_state;
};

struct LandmarkPoint
{
	LandmarkPoint();

	uint32 Id;
	std::string name;
	uint32 map_id;
	uint32 map_type;
	float x, y, z;
};
