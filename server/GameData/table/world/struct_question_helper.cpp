#include "jsontable/table_helper.h"
#include "struct_question.h"

template<> const char *GetTableName<QuestQuestion>()
{
	return "quest_question";
}

template<> const char *GetTableKeyName<QuestQuestion>()
{
	return "Id";
}

template<> uint64 GetTableKeyValue(const QuestQuestion &entity)
{
	return (uint64)entity.Id;
}

template<> void SetTableKeyValue(QuestQuestion &entity, uint64 key)
{
	entity.Id = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<QuestQuestion>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<QuestQuestion>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	return -1;
}

template<> size_t GetTableFieldNumber<QuestQuestion>()
{
	return 1;
}

template<> std::string GetTableFieldValue(const QuestQuestion &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
	}
	return "";
}

template<> void SetTableFieldValue(QuestQuestion &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
	}
}

template<> void LoadFromStream(QuestQuestion &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
}

template<> void SaveToStream(const QuestQuestion &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
}

template<> void LoadFromText(QuestQuestion &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestQuestion &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestQuestion &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
}

template<> void JsonHelper::BlockToJson(const QuestQuestion &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
}
