#include "jsontable/table_helper.h"
#include "struct_sobj.h"

SObjPrototype::Flags::Flags()
: isExclusiveTrigger(false)
, isRemoveAfterTriggerDone(false)
, isPlayerInteresting(false)
, isCreatureInteresting(false)
, isActivity(false)
{
}

SObjPrototype::CostItem::CostItem()
: itemId(0)
, itemNum(0)
{
}

SObjPrototype::SObjPrototype()
: sobjTypeId(0)
, minRespawnTime(0)
, maxRespawnTime(0)
, teleportPointID(0)
, teleportDelayTime(0)
, lootSetID(0)
, triggerSpellID(0)
, triggerSpellLv(0)
, triggerAnimTime(0)
, sobjReqQuestDoing(0)
, radius(.0f)
, spawnScriptId(0)
, playScriptId(0)
{
}

StaticObjectSpawn::Flags::Flags()
: isRespawn(false)
, isPlaceholder(false)
{
}

StaticObjectSpawn::StaticObjectSpawn()
: spawnId(0)
, entry(0)
, map_id(0)
, map_type(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, radius(.0f)
{
}
