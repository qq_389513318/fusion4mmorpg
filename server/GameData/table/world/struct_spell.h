#pragma once

enum class SpellStageType
{
	Chant,
	Channel,
	Cleanup,
};

enum class SpellResumeType
{
	RemoveWhenOffline,
	ContinueWhenOnline,
	ContinueByTime,
};

enum class SpellEffectStyle
{
	None,
	Positive,
	Negative,
	All,
};

enum class SpellInterruptBy
{
	None,
	Move,
	Injured,
};

enum class SpellEffectType
{
	None = 0,
	Attack,
	AuraAttack,
	ChangeProp,
	ChangeSpeed,
	Invincible,
	Invisible,
	Fetter,
	Stun,

	AuraObject = 300,

	Teleport = 500,
	Mine,
};

enum class SpellTargetType
{
	Any,
	Friend,
	Enemy,
	TeamMember,
	Count,
};

enum class SpellSelectType
{
	None,
	Self,
	Friend,
	Enemy,
	Friends,
	Enemies,
	SelfOrFriend,
	FocusFriends,
	FocusEnemies,
	TeamMember,
	TeamMembers,
	SelfOrTeamMember,
	FocusTeamMembers,
	Reference,
	Count,
};

enum class SpellSelectMode
{
	Circle,
	Sector,
	Rect,
	Count,
};

enum class SpellPassiveMode
{
	Status,
	Event,
	Count,
};

enum class SpellPassiveBy
{
	StatusNone = 0,
	StatusHPValue,
	StatusHPRate,
	StatusMax,

	EventHit = 0,
	EventHitBy,
	EventMax,
};

struct SpellInfo
{
	SpellInfo();

	struct Flags {
		Flags();
		bool isExclusive;
		bool isPassive;
		bool isAppearance;
		bool isSync2Client;
		bool isSync2AllClient;
		bool isTargetDeadable;
		bool isCheckCastDist;
		bool isIgnoreOutOfControl;
	};
	uint32 spellID;
	std::string spellIcon;
	Flags spellFlags;
	uint8 spellBuffType;
	uint8 spellCooldownType;
	uint8 spellTargetType;
	uint8 spellPassiveMode;
	uint8 spellPassiveBy;
	std::vector<float> spellPassiveArgs;
	float spellPassiveChance;
	uint32 spellLimitCareer;
	uint32 spellLimitMapType;
	uint32 spellLimitMapID;
	uint32 spellLimitScriptID;
	std::string spellLimitScriptArgs;
	uint32 spellInterruptBys;
	std::vector<uint32> spellStageTime;
};

struct SpellLevelInfo
{
	SpellLevelInfo();

	uint32 spellLevelID;
	uint32 spellID;
	uint32 spellLimitLevel;
	float spellCastDistMin;
	float spellCastDistMax;
	uint32 spellEvalScore;
	uint32 spellCostHP;
	float spellCostHPRate;
	uint32 spellCostMP;
	float spellCostMPRate;
	uint32 spellCDTime;
};

struct SpellLevelEffectInfo
{
	SpellLevelEffectInfo();

	struct Flags {
		Flags();
		bool isResumable;
		bool isDeadResumable;
		bool isSync2Client;
		bool isSync2AllClient;
		bool isTargetDeadable;
		bool isClientSelectTarget;
	};
	uint32 spellLevelEffectID;
	uint32 spellID;
	uint32 spellLevelID;
	Flags spellEffectFlags;
	uint8 spellStageTrigger;
	uint32 spellDelayTrigger;
	uint8 spellSelectType;
	uint8 spellSelectMode;
	std::vector<float> spellSelectArgs;
	uint32 spellSelectNumber;
	uint32 spellDelayEffective;
	float spellEffectiveChance;
	uint32 spellEffectStyle;
	uint32 spellEffectType;
	std::string spellEffectArgs;
};

enum class AuraSelectType
{
	Friend,
	Enemy,
	Player,
	Creature,
	TeamMember,
	Count,
};
