table EditorTreeView
{
	enum Type
	{
		None,
		Spell,
		Quest,
		Char,
		SObj,
		Item,
		Loot,
	}
	required uint32 etvType;
	required string etvTree;
	(key=etvType, tblname=editor_tree_view)
}