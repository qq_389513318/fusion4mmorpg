table GameTime
{
	enum Type {
		None,
		PlayBoss,
		GuildLeague,
	}
	required uint32 Id;
	required string strTime;
	required string strDesc;
	(key=Id,tblname=game_time)
}