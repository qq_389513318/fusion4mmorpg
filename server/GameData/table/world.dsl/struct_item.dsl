enum ItemClass
{
	None,
	Equip,  // 装备
	Quest,  // 任务
	Material,  // 材料
	Gemstone,  // 宝石
	Consumable,  // 消耗品
	Score,  // 积分
	Scrap,  // 废品
	Other,  // 其它
}

enum ItemSubClass
{
	Equip_Weapon = 1,  // 武器
	Equip_Belt,  // 腰带
	Equip_Gloves,  // 手套
	Equip_Shoes,  // 鞋子
}

enum ItemQuality
{
	White,
	Red,
	Count
}

table ItemPrototype
{
	struct Flags {
		bool canUse;
		bool canSell;
		bool canDestroy;
		bool isDestroyAfterUse;
	};

	required uint32 itemTypeID;
	required Flags itemFlags;
	required uint32 itemClass;
	required uint32 itemSubClass;
	required uint32 itemQuality;
	required uint32 itemLevel;
	required uint32 itemStack;

	required uint32 itemSellPrice;

	required uint32 itemLootId;
	required uint32 itemSpellId;
	required uint32 itemSpellLevel;
	required uint32 itemScriptId;
	required string itemScriptArgs;
	required uint32[] itemParams;

	(key=itemTypeID, tblname=item_prototype)
}

table ItemEquipPrototype
{
	required uint32 itemTypeID;

	struct Attr {
		uint32 type;
		double value;
	};
	required Attr[] itemEquipAttrs;

	struct Spell {
		uint32 id;
		uint32 level;
	};
	required Spell[] itemEquipSpells;

	(key=itemTypeID, tblname=item_equip_prototype)
}