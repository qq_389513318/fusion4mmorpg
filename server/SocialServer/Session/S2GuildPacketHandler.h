#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GuildMgr;

class S2GuildPacketHandler :
	public MessageHandler<S2GuildPacketHandler, GuildMgr, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<S2GuildPacketHandler>
{
public:
	S2GuildPacketHandler();
	virtual ~S2GuildPacketHandler();
private:
	int HandleUpdateGameServer(GuildMgr *mgr, INetPacket &pck);
	int HandleUpdateCharacterInfo(GuildMgr *mgr, INetPacket &pck);
	int HandlePullGuildInfos(GuildMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGetAllGuilds(GuildMgr *mgr, INetPacket &pck);
	int HandleGetAllCharacterGuilds(GuildMgr *mgr, INetPacket &pck);
	int HandleTryGuildCreate(GuildMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGuildCreate(GuildMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGuildInvite(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildInviteResp(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildApply(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildApplyResp(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildLeave(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildKick(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildRise(GuildMgr *mgr, INetPacket &pck);
	int HandleGuildDisband(GuildMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGuildGetApply(GuildMgr *mgr, INetPacket &pck);
};

#define sS2GuildPacketHandler (*S2GuildPacketHandler::instance())
