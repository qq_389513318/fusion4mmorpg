#include "preHeader.h"
#include "GameServerMgr.h"

GameServerMgr::GameServerMgr()
: m_serverId(0)
, m_startServerTime(-1)
, m_pGameServerSession(NULL)
{
}

GameServerMgr::~GameServerMgr()
{
}

bool GameServerMgr::RegisterGameServer(GameServerSession* pSession)
{
	if (pSession == NULL) {
		return false;
	}
	if (m_pGameServerSession != NULL) {
		return false;
	}

	m_pGameServerSession = pSession;
	NLOG("Register Game Server <%p>!", pSession);

	return true;
}

void GameServerMgr::RemoveGameServer(GameServerSession* pSession)
{
	if (pSession == NULL) {
		return;
	}
	if (m_pGameServerSession != pSession) {
		return;
	}

	m_pGameServerSession = NULL;
	WLOG("Remove Game Server <%p>!", pSession);
}

void GameServerMgr::KickPlayer(uint32 uid, GErrorCode error)
{
	NetPacket pack(SGX_KICK);
	pack << uid << (s32)error;
	SendPacket2GS(pack);
}

void GameServerMgr::SendPacket2GS(const INetPacket& pck)
{
	if (m_pGameServerSession != NULL) {
		m_pGameServerSession->PushSendPacket(pck);
	}
}

void GameServerMgr::SendPacket2GS(const INetPacket& pck, const INetPacket& data)
{
	if (m_pGameServerSession != NULL) {
		m_pGameServerSession->PushSendPacket(pck, data);
	}
}

void GameServerMgr::SendPacket2AllMS(const INetPacket& pck)
{
	NetPacket toGS(SGX_TO_ALL_MAP_SERVER_PACKET);
	SendPacket2GS(toGS, pck);
}

void GameServerMgr::SendPacket2Instance(InstGUID instGuid, const INetPacket& pck)
{
	NetPacket toGS(SGX_TO_INSTANCE_PACKET);
	toGS << instGuid;
	SendPacket2GS(toGS, pck);
}

void GameServerMgr::SendPacket2Player(uint32 uid, const INetPacket& pck)
{
	NetPacket toGS(SGX_TO_PLAYER_PACKET);
	toGS << uid;
	SendPacket2GS(toGS, pck);
}

RPCError GameServerMgr::RPCInvoke2GS(const INetPacket &pck,
	const std::function<void(INetStream&, int32, bool)> &cb,
	AsyncTaskOwner *owner, time_t timeout)
{
	if (m_pGameServerSession != NULL) {
		m_pGameServerSession->RPCInvoke(pck, cb, owner, timeout);
		return RPCErrorNone;
	} else {
		return RPCErrorCancel;
	}
}

void GameServerMgr::RPCReply2GS(
	const INetPacket &pck, uint64 sn, int32 err, bool eof)
{
	if (m_pGameServerSession != NULL) {
		m_pGameServerSession->RPCReply(pck, sn, err, eof);
	}
}

RPCError GameServerMgr::RPCInvoke2Instance(
	InstGUID instGuid, const INetPacket& pck,
	const std::function<void(INetStream&, int32, bool)>& cb,
	AsyncTaskOwner* owner, time_t timeout)
{
	if (m_pGameServerSession != NULL) {
		NetPacket toGS(SGX_TO_INSTANCE_PACKET);
		toGS << instGuid;
		m_pGameServerSession->RPCTransInvoke(toGS, pck, cb, owner, timeout);
		return RPCErrorNone;
	} else {
		return RPCErrorCancel;
	}
}

void GameServerMgr::RPCReply2MS4Instance(InstGUID instGuid,
	const INetPacket &pck, uint64 sn, int32 err, bool eof)
{
	if (m_pGameServerSession != NULL) {
		NetPacket toGS(SGX_TO_MAP_SERVER_INSTANCE_PACKET);
		toGS << instGuid;
		m_pGameServerSession->RPCTransReply(toGS, pck, sn, err, eof);
	}
}

RPCError GameServerMgr::RPCInvoke2Player(
	uint32 uid, const INetPacket& pck,
	const std::function<void(INetStream&, int32, bool)>& cb,
	AsyncTaskOwner* owner, time_t timeout)
{
	if (m_pGameServerSession != NULL) {
		NetPacket toGS(SGX_TO_PLAYER_PACKET);
		toGS << uid;
		m_pGameServerSession->RPCTransInvoke(toGS, pck, cb, owner, timeout);
		return RPCErrorNone;
	} else {
		return RPCErrorCancel;
	}
}

void GameServerMgr::RPCReply2MS4Player(uint32 uid,
	const INetPacket &pck, uint64 sn, int32 err, bool eof)
{
	if (m_pGameServerSession != NULL) {
		NetPacket toGS(SGX_TO_MAP_SERVER_PLAYER_PACKET);
		toGS << uid;
		m_pGameServerSession->RPCTransReply(toGS, pck, sn, err, eof);
	}
}
