#pragma once

#include "rpc/RPCSession.h"

class GameServerSessionHandler;

class GameServerSession : public RPCSession
{
public:
	GameServerSession();
	virtual ~GameServerSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnShutdownSession();

	bool IsReady() const { return m_isReady; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void BroadcastPacket2AllServices(const INetPacket &pck);

	friend GameServerSessionHandler;
	bool m_isReady;
};
