#include "preHeader.h"
#include "GateServerListener.h"
#include "GateServerSession.h"
#include "ServerMaster.h"

GateServerListener::GateServerListener()
: m_sn(0)
{
}

GateServerListener::~GateServerListener()
{
}

std::string GateServerListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GATE_SERVER", "HOST", "127.0.0.1");
}

std::string GateServerListener::GetBindPort()
{
	return "0";
}

Session *GateServerListener::NewSessionObject()
{
	return new GateServerSession(++m_sn);
}
