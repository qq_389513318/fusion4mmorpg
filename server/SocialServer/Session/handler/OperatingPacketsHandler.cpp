#include "preHeader.h"
#include "Operating/OperatingActivityMgr.h"
#include "Session/C2OperatingPacketHandler.h"
#include "Session/S2OperatingPacketHandler.h"

int C2OperatingPacketHandler::HandleOperatingGetType(OperatingActivityMgr *mgr, INetPacket &pck, uint32 uid)
{
	mgr->BuildTotalActivityTypePacket(uid);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingGetList(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	int32 actUIType;
	pck >> actUIType;
	mgr->BuildActivityListPacket(playerInfo, actUIType);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingGetDetail(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint64 actUniqueKey;
	pck >> actUniqueKey;
	mgr->BuildActivityDetailPacket(playerInfo, actUniqueKey);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingGetShortHot(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint64 actUniqueKey;
	pck >> actUniqueKey;
	mgr->BuildActivityShortShotPacket(playerInfo, actUniqueKey);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingBuy(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint64 actUniqueKey;
	uint32 index;
	pck >> actUniqueKey >> index;
	mgr->OnPlayerBuy(playerInfo, actUniqueKey, index);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingGetReward(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint64 actUniqueKey;
	uint32 index;
	pck >> actUniqueKey >> index;
	mgr->OnPlayerGetReward(playerInfo, actUniqueKey, index);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingLogin(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	mgr->OnPlayerLogin(playerInfo);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingPay(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint32 rmbVal, goldVal, payType;
	pck >> rmbVal >> goldVal >> payType;
	mgr->OnPlayerPay(playerInfo, rmbVal, goldVal, payType);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingCost(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint32 goldVal, costType;
	pck >> goldVal >> costType;
	mgr->OnPlayerCost(playerInfo, goldVal, costType);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingGainScore(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint32 scoreValue, scoreType;
	pck >> scoreValue >> scoreType;
	mgr->OnPlayerGainScore(playerInfo, scoreValue, scoreType);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingPlayActvt(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint32 playType, params[8];
	pck >> playType;
	for (int i = 0; i < ARRAY_SIZE(params) && !pck.IsReadableEmpty(); ++i) {
		pck >> params[i];
	}
	mgr->OnPlayerPlayActvt(playerInfo, (OperatingPlayType)playType, params);
	return SessionHandleSuccess;
}

int S2OperatingPacketHandler::HandleOperatingEventActvt(OperatingActivityMgr *mgr, INetPacket &pck)
{
	auto playerInfo = OperatingActivityMgr::UnpackPlayerInfo(pck);
	uint32 eventType, params[8];
	pck >> eventType;
	for (int i = 0; i < ARRAY_SIZE(params) && !pck.IsReadableEmpty(); ++i) {
		pck >> params[i];
	}
	mgr->OnPlayerEventActvt(playerInfo, (OperatingEventType)eventType, params);
	return SessionHandleSuccess;
}
