#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/OpCode.h"

class OperatingActivityMgr;

class C2OperatingPacketHandler :
	public MessageHandler<C2OperatingPacketHandler, OperatingActivityMgr, GAME_OPCODE::CSMSG_COUNT, uint32>,
	public Singleton<C2OperatingPacketHandler>
{
public:
	C2OperatingPacketHandler();
	virtual ~C2OperatingPacketHandler();
private:
	int HandleOperatingGetType(OperatingActivityMgr *mgr, INetPacket &pck, uint32 uid);
};

#define sC2OperatingPacketHandler (*C2OperatingPacketHandler::instance())
