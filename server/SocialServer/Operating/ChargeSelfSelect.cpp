#include "preHeader.h"
#include "ChargeSelfSelect.h"

ChargeSelfSelect::ChargeSelfSelect(const OperatingActivity* pProto)
: OperatingActivityBase(pProto, nSaveData)
{
}

ChargeSelfSelect::~ChargeSelfSelect()
{
}

GErrorCode ChargeSelfSelect::OnPlayerGetReward(INetPacket& pck, const OperatingPlayerInfo& info, uint32 index)
{
	auto pActData = GetActivityData(info.playerId);
	if (pActData == NULL) {
		return ErrOperatingNotReachTarget;
	}

	if (pActData->saveData[DrawTimes] >= m_actvtParams.maxTimes) {
		return ErrOperatingNothingMore;
	}
	if (pActData->saveData[DrawTimes] >= GetTotalRewardTimes(pActData)) {
		return ErrOperatingNotReachTarget;
	}

	if (index >= m_actvtParams.rewardDataList.size()) {
		return ErrOperatingInvalidReward;
	}

	auto& rewardData = m_actvtParams.rewardDataList[index];
	auto errCode = GetRewardItems(
		info, rewardData.rewardItems, false, 1, [=]() {
			pActData->saveData[DrawTimes] -= 1;
			return true;
		});
	if (errCode != CommonSuccess) {
		return errCode;
	}

	pActData->saveData[DrawTimes] += 1;
	SaveDataToDB(pActData->playerId);

	return CommonWaiting;
}

bool ChargeSelfSelect::OnPlayerPay(const OperatingPlayerInfo& info, uint32 rmbVal, uint32 goldVal, uint32 payId)
{
	auto pActData = CreateAndGetActivityData(info.playerId);
	pActData->saveData[PayValue] += rmbVal;
	SaveDataToDB(pActData->playerId);
	return true;
}

void ChargeSelfSelect::OnActivityEnd()
{
	auto& rewardItems = m_actvtParams.rewardDataList.front().rewardItems;
	for (auto& [playerId, pActData] : m_mapActData) {
		auto totalTimes = GetTotalRewardTimes(pActData);
		auto restTimes = SubLeastZero(totalTimes, pActData->saveData[DrawTimes]);
		if (restTimes > 0) {
			SendRewardMail(playerId, rewardItems,
				400200286, 400200287, emptyStringView, emptyStringView, restTimes);
		}
	}
	OperatingActivityBase::OnActivityEnd();
}

bool ChargeSelfSelect::HasShortHot(const OperatingPlayerInfo& info) const
{
	auto pActData = GetActivityData(info.playerId);
	if (pActData == NULL) {
		return false;
	}
	auto totalTimes = GetTotalRewardTimes(pActData);
	if (pActData->saveData[DrawTimes] < totalTimes) {
		return true;
	}
	return false;
}

void ChargeSelfSelect::LoadActivityCares(std::bitset<(int)OperatingCareType::Count>& cares)
{
	cares.set((int)OperatingCareType::Pay);
}

uint32 ChargeSelfSelect::GetTotalRewardTimes(const OperatingActivityData* pActData) const
{
	return std::min(pActData->saveData[PayValue] / m_actvtParams.gradVal, m_actvtParams.maxTimes);
}

void ChargeSelfSelect::ReloadActivityParams()
{
	REINIT_OBJECT(&m_actvtParams);
	TextUnpacker unpacker(m_pProto->strActivityParams.c_str());
	unpacker >> m_actvtParams.gradVal >> m_actvtParams.maxTimes;
	do {
		OperatingChargeSelfSelect::RewardData rewardData;
		do {
			FItemInfo rewardItem;
			LoadConfigByFormat("uuu", rewardItem, unpacker);
			rewardData.rewardItems.push_back(rewardItem);
		} while (!unpacker.IsEmpty() && !unpacker.IsDelimiter('|'));
		m_actvtParams.rewardDataList.push_back(std::move(rewardData));
	} while (!unpacker.IsEmpty());
}

void ChargeSelfSelect::BuildActivityParamsPacket(NetPacket& pck, const OperatingPlayerInfo& info)
{
	SaveToINetStream(m_actvtParams, pck);
}
