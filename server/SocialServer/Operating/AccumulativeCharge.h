#pragma once

#include "OperatingActivityBase.h"

class AccumulativeCharge : public OperatingActivityBase
{
public:
	AccumulativeCharge(const OperatingActivity* pProto);
	virtual ~AccumulativeCharge();

	virtual GErrorCode OnPlayerGetReward(INetPacket& pck, const OperatingPlayerInfo& info, uint32 index);
	virtual bool OnPlayerPay(const OperatingPlayerInfo& info, uint32 rmbVal, uint32 goldVal, uint32 payType);
	virtual void OnDayEnd();

	virtual bool HasShortHot(const OperatingPlayerInfo& info) const;

	virtual void LoadActivityCares(std::bitset<(int)OperatingCareType::Count>& cares);

private:
	virtual void ReloadActivityParams();
	virtual void BuildActivityParamsPacket(NetPacket& pck, const OperatingPlayerInfo& info);

	enum {
		PayValue,
		DrawFlags,
		nSaveData,
	};
	OperatingAccumulativeCharge m_actvtParams;
};
