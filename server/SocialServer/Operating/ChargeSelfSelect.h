#pragma once

#include "OperatingActivityBase.h"

class ChargeSelfSelect : public OperatingActivityBase
{
public:
	ChargeSelfSelect(const OperatingActivity* pProto);
	virtual ~ChargeSelfSelect();

	virtual GErrorCode OnPlayerGetReward(INetPacket& pck, const OperatingPlayerInfo& info, uint32 index);
	virtual bool OnPlayerPay(const OperatingPlayerInfo& info, uint32 rmbVal, uint32 goldVal, uint32 payType);
	virtual void OnActivityEnd();

	virtual bool HasShortHot(const OperatingPlayerInfo& info) const;

	virtual void LoadActivityCares(std::bitset<(int)OperatingCareType::Count>& cares);

private:
	virtual void ReloadActivityParams();
	virtual void BuildActivityParamsPacket(NetPacket& pck, const OperatingPlayerInfo& info);

	uint32 GetTotalRewardTimes(const OperatingActivityData* pActData) const;

	enum {
		PayValue,
		DrawTimes,
		nSaveData,
	};
	OperatingChargeSelfSelect m_actvtParams;
};
