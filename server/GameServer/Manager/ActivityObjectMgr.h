#pragma once

#include "Singleton.h"
#include "rpc/RPCActor.h"

class MapServerSession;

class ActivityObjectMgr : public Singleton<ActivityObjectMgr>
{
public:
	ActivityObjectMgr();
	virtual ~ActivityObjectMgr();

	GErrorCode OnUpdateActivityObjectPosition(INetPacket& pck);
	GErrorCode OnQueryActivityObjectPosition(INetPacket& pck,
		MapServerSession *pSession, const RPCActor::RequestMetaInfo &info) const;

private:
	struct ActObjPos {
		ObjGUID objGuid;
		InstGUID instGuid;
		vector3f pos;
	};
	enum class ActObjType {
		None,
		StaticObject,
		Creature,
		Count
	};

	static const ActObjPos* FilterBestActObjPos(
		const std::vector<ActObjPos>& actObjPosList, InstGUID instGuid);
	static ActObjType ObjectType2ActObjType(OBJECT_TYPE objType);

	std::unordered_map<uint32, std::vector<ActObjPos>> m_actObjPos[(int)ActObjType::Count];
};

#define sActivityObjectMgr (*ActivityObjectMgr::instance())
