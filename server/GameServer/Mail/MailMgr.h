#pragma once

#include "Singleton.h"

class MailMgr : public WheelTimerOwner, public Singleton<MailMgr>
{
public:
	MailMgr();
	virtual ~MailMgr();

	void Init();

	GErrorCode SendMail(const inst_mail& mailProp);

	GErrorCode HandleMailRequest(INetStream& pck);

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	void AcquireMailExpireTime();
	void DeleteMailExpireTime();

	GErrorCode HandleSendSingleMail(INetStream& pck);
	GErrorCode HandleSendMultiSameMail(INetStream& pck);
	GErrorCode HandleSendMultiDifferentMail(INetStream& pck);
};

#define sMailMgr (*MailMgr::instance())
