#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class MapServerSession;

class MapServerSessionHandler :
	public MessageHandler<MapServerSessionHandler, MapServerSession, MapServer2GameServer::MAP2GAME_MESSAGE_COUNT>,
	public Singleton<MapServerSessionHandler>
{
public:
	MapServerSessionHandler();
	virtual ~MapServerSessionHandler();
private:
	int HandleRegister(MapServerSession *pSession, INetPacket &pck);
	int HandleToSocialServerPacket(MapServerSession *pSession, INetPacket &pck);
	int HandleKick(MapServerSession *pSession, INetPacket &pck);
	int HandleCharacterSaved(MapServerSession *pSession, INetPacket &pck);
	int HandleUpdateCharacterInfo(MapServerSession *pSession, INetPacket &pck);
	int HandleSwitchMap(MapServerSession *pSession, INetPacket &pck);
	int HandleCharacterTeleportBeginEnterInstanceResult(MapServerSession *pSession, INetPacket &pck);
	int HandleCharacterEnterMapResp(MapServerSession *pSession, INetPacket &pck);
	int HandleCharacterLeaveMapResp(MapServerSession *pSession, INetPacket &pck);
	int HandleUpdateActivityObjectPosition(MapServerSession *pSession, INetPacket &pck);
	int HandleQueryActivityObjectPosition(MapServerSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleMapTeamCreate(MapServerSession *pSession, INetPacket &pck);
	int HandleMail(MapServerSession *pSession, INetPacket &pck);
	int HandleFinishGuildLeague(MapServerSession *pSession, INetPacket &pck);
};

#define sMapServerSessionHandler (*MapServerSessionHandler::instance())
