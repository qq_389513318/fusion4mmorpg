#include "preHeader.h"
#include "GateServerMgr.h"
#include "Game/AccountMgr.h"

GateServerMgr::GateServerMgr()
{
}

GateServerMgr::~GateServerMgr()
{
}

void GateServerMgr::AddGateServer(GateServerSession* pSession)
{
	m_GateServerMap.emplace(pSession->sn(), pSession);
}

void GateServerMgr::RemoveGateServer(GateServerSession* pSession)
{
	m_GateServerMap.erase(pSession->sn());
	auto& infoMap = sAccountMgr.GetAccountInfoMap();
	for (auto itr = infoMap.begin(); itr != infoMap.end();) {
		auto&[uid, pAccount] = *itr++;
		if (pAccount->GetSession() == pSession) {
			sAccountMgr.KillAccount(uid);
		}
	}
}

void GateServerMgr::BroadcastServerId2AllGateServer(uint32 serverId) const
{
	NetPacket pck(SGG_PUSH_SERVER_ID);
	pck << serverId;
	BroadcastPacket2AllGateServer(pck);
}

void GateServerMgr::BroadcastSocialListen2AllGateServer(const std::string& addr, const std::string& port) const
{
	NetPacket pck(SGG_PUSH_SOCIAL_LISTEN);
	pck << addr << port;
	BroadcastPacket2AllGateServer(pck);
}

void GateServerMgr::BroadcastPacket2AllClient(const INetPacket& data) const
{
	NetPacket pck(SGG_PUSH_PACKET_TO_ALL_CLIENT);
	BroadcastPacket2AllGateServer(pck, data);
}

void GateServerMgr::BroadcastPacket2AllGateServer(const INetPacket& pck) const
{
	for (const auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck);
	}
}

void GateServerMgr::BroadcastPacket2AllGateServer(const INetPacket& pck, const INetPacket& data) const
{
	for (const auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck, data);
	}
}
