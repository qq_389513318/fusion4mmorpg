#include "preHeader.h"
#include "ClientPacketHandler.h"
#include "protocol/OpCode.h"

ClientPacketHandler::ClientPacketHandler()
{
	handlers_[GAME_OPCODE::CMSG_CHARACTER_CREATE] = &ClientPacketHandler::HandleCharacterCreate;
	handlers_[GAME_OPCODE::CMSG_CHARACTER_DELETE] = &ClientPacketHandler::HandleCharacterDelete;
	handlers_[GAME_OPCODE::CMSG_CHARACTER_LIST] = &ClientPacketHandler::HandleCharacterList;
	handlers_[GAME_OPCODE::CMSG_CHARACTER_LOGIN] = &ClientPacketHandler::HandleCharacterLogin;
	handlers_[GAME_OPCODE::CMSG_CHARACTER_LOGOUT] = &ClientPacketHandler::HandleCharacterLogout;
	handlers_[GAME_OPCODE::CMSG_PLAYER_LOAD_MAP_FINISH] = &ClientPacketHandler::HandlePlayerLoadMapFinish;
	handlers_[GAME_OPCODE::CMSG_CHAT_MESSAGE] = &ClientPacketHandler::HandleChatMessage;
	handlers_[GAME_OPCODE::CMSG_TEAM_CREATE] = &ClientPacketHandler::HandleTeamCreate;
	handlers_[GAME_OPCODE::CMSG_TEAM_INVITE] = &ClientPacketHandler::HandleTeamInvite;
	handlers_[GAME_OPCODE::CMSG_TEAM_INVITE_RESP] = &ClientPacketHandler::HandleTeamInviteResp;
	handlers_[GAME_OPCODE::CMSG_TEAM_APPLY] = &ClientPacketHandler::HandleTeamApply;
	handlers_[GAME_OPCODE::CMSG_TEAM_APPLY_RESP] = &ClientPacketHandler::HandleTeamApplyResp;
	handlers_[GAME_OPCODE::CMSG_TEAM_LEAVE] = &ClientPacketHandler::HandleTeamLeave;
	handlers_[GAME_OPCODE::CMSG_TEAM_KICK] = &ClientPacketHandler::HandleTeamKick;
	handlers_[GAME_OPCODE::CMSG_TEAM_TRANSFER] = &ClientPacketHandler::HandleTeamTransfer;
	handlers_[GAME_OPCODE::CMSG_GUILD_CREATE] = &ClientPacketHandler::HandleGuildCreate;
	handlers_[GAME_OPCODE::CMSG_GUILD_INVITE] = &ClientPacketHandler::HandleGuildInvite;
	handlers_[GAME_OPCODE::CMSG_GUILD_INVITE_RESP] = &ClientPacketHandler::HandleGuildInviteResp;
	handlers_[GAME_OPCODE::CMSG_GUILD_APPLY] = &ClientPacketHandler::HandleGuildApply;
	handlers_[GAME_OPCODE::CMSG_GUILD_APPLY_RESP] = &ClientPacketHandler::HandleGuildApplyResp;
	handlers_[GAME_OPCODE::CMSG_GUILD_LEAVE] = &ClientPacketHandler::HandleGuildLeave;
	handlers_[GAME_OPCODE::CMSG_GUILD_KICK] = &ClientPacketHandler::HandleGuildKick;
	handlers_[GAME_OPCODE::CMSG_GUILD_RISE] = &ClientPacketHandler::HandleGuildRise;
	handlers_[GAME_OPCODE::CMSG_GUILD_DISBAND] = &ClientPacketHandler::HandleGuildDisband;
	handlers_[GAME_OPCODE::CMSG_GUILD_GET_APPLY] = &ClientPacketHandler::HandleGuildGetApply;
	handlers_[GAME_OPCODE::CMSG_GET_SHOP_ITEM_LIST] = &ClientPacketHandler::HandleGetShopItemList;
	handlers_[GAME_OPCODE::CMSG_BUY_SPECIAL_SHOP_ITEM] = &ClientPacketHandler::HandleBuySpecialShopItem;
	handlers_[GAME_OPCODE::CMSG_PLAYBOSS_JOIN] = &ClientPacketHandler::HandlePlaybossJoin;
};

ClientPacketHandler::~ClientPacketHandler()
{
}
