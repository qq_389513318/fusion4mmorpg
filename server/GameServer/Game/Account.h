#pragma once

class GateServerSession;
class Character;

class Account
{
public:
	Account(GateServerSession* pSession, uint32 logicGsID, uint32 uid, uint32 sn);
	~Account();

	const uint32 logicGsID;
	const uint32 uid;
	const uint32 sn;

	bool isCharLoginning;

	void SendError(GErrorCode error) const;
	void SendError(const GErrorCodeP1& error) const;
	void SendError(const GErrorCodeP2& error) const;
	void SendError(const GErrorCodeP3& error) const;
	void SendError(const GErrorInfo& error) const;
	void SendRespError(uint32 respOpCode, const GErrorInfo& error) const;

	void SendPacket(const INetPacket& pck) const;
	void SendPacket2GateServer(const INetPacket& pck) const;

	void SetCharacter(Character* pChar);
	Character* GetCharacter() const { return m_pChar; }
	bool IsCharacterOnline() const { return m_pChar != NULL; }

	GateServerSession* GetSession() const { return m_pSession; }

private:
	GateServerSession* const m_pSession;
	Character* m_pChar;
};
