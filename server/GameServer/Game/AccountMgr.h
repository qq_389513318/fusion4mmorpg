#pragma once

#include "Singleton.h"
#include "Account.h"

class AccountMgr : public Singleton<AccountMgr>
{
public:
	AccountMgr();
	virtual ~AccountMgr();

	Account* GetAccount(uint32 uid) const;

	GErrorCode AddAccount(GateServerSession* pSession,
		uint32 logicGsID, uint32 uid, uint32 sn);
	GErrorCode KickAccount(uint32 uid, GErrorCode error);
	GErrorCode KillAccount(uint32 uid);

	auto GetAccountInfoMap() const ->
		const std::unordered_map<uint32, Account*>& {
		return m_AccountInfoMap;
	}

private:
	GErrorCode RemoveAccount(uint32 uid,
		uint32 opcode, const std::string_view& args = emptyStringView);

	std::unordered_map<uint32, Account*> m_AccountInfoMap;
};

#define sAccountMgr (*AccountMgr::instance())
