#pragma once

#include "Singleton.h"
#include "rpc/RPCSession.h"

class CenterSessionHandler;

class CenterSession : public RPCSession, public Singleton<CenterSession>
{
public:
	CenterSession();
	virtual ~CenterSession();

	void CheckConnection();

	void SendPacket(const INetPacket& pck);

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void OnShutdownSession();
	virtual void DeleteObject();

private:
	friend CenterSessionHandler;
	std::list<INetPacket*> m_toSendPckList;
};

#define sCenterSession (*CenterSession::instance())
