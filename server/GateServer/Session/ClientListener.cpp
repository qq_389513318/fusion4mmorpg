#include "preHeader.h"
#include "ClientListener.h"
#include "ClientSession.h"
#include "ServerMaster.h"

ClientListener::ClientListener()
: m_sn(0)
{
}

ClientListener::~ClientListener()
{
}

bool ClientListener::BindSocketReady(SOCKET sockfd)
{
	if (!OS::reuse_port(sockfd)) {
		ELOG("reuse_port(), errno: %d.", GET_SOCKET_ERROR());
		return false;
	}
	return true;
}

std::string ClientListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GATE_SERVER", "HOST", "0.0.0.0");
}

std::string ClientListener::GetBindPort()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GATE_SERVER", "PORT", "9999");
}

Session *ClientListener::NewSessionObject()
{
	return new ClientSession(++m_sn);
}
