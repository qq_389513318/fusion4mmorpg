#include "preHeader.h"
#include "MapServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

MapServerSessionHandler::MapServerSessionHandler()
{
	handlers_[GateServer2MapServer::SGM_REGISTER_RESP] = &MapServerSessionHandler::HandleRegisterResp;
	handlers_[GateServer2MapServer::SGM_PUSH_PACKET_TO_ALL_CLIENT] = &MapServerSessionHandler::HandlePushPacketToAllClient;
};

MapServerSessionHandler::~MapServerSessionHandler()
{
}
