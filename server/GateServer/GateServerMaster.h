#pragma once

#include "Singleton.h"
#include "ServerMaster.h"

class GateServerMaster : public IServerMaster, public Singleton<GateServerMaster>
{
public:
	GateServerMaster();
	virtual ~GateServerMaster();

	virtual bool InitSingleton();
	virtual void FinishSingleton();

private:
	virtual bool InitDBPool();
	virtual bool LoadDBData();

	virtual bool StartServices();
	virtual void StopServices();

	virtual void Tick();

	virtual std::string GetConfigFile();
	virtual size_t GetDeferAsyncServiceCount();
	virtual size_t GetAsyncServiceCount();
	virtual size_t GetIOServiceCount();
};

#define sGateServerMaster (*GateServerMaster::instance())
