#include "preHeader.h"
#include "CrossServerPacketHandler.h"
#include "protocol/InternalProtocol.h"

CrossServerPacketHandler::CrossServerPacketHandler()
{
	handlers_[MapServer2CrossServer::CMC_PLAYBOSS_CHANGE_TEAM] = &CrossServerPacketHandler::HandlePlaybossChangeTeam;
	handlers_[MapServer2CrossServer::CMC_PLAYBOSS_BROADCAST] = &CrossServerPacketHandler::HandlePlaybossBroadcast;
	handlers_[MapServer2CrossServer::CMC_PLAYBOSS_STRIKE] = &CrossServerPacketHandler::HandlePlaybossStrike;
	handlers_[MapServer2CrossServer::CMC_PLAYBOSS_SYNC] = &CrossServerPacketHandler::HandlePlaybossSync;
	handlers_[MapServer2CrossServer::CMC_PLAYBOSS_JOIN] = &CrossServerPacketHandler::HandlePlaybossJoin;
};

CrossServerPacketHandler::~CrossServerPacketHandler()
{
}
