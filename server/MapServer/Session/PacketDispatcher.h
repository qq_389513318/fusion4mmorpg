#pragma once

#include "Singleton.h"
#include "rpc/RPCManager.h"

class PacketDispatcher : private RPCActor, public Singleton<PacketDispatcher>
{
public:
	PacketDispatcher();
	virtual ~PacketDispatcher();

	void Tick();

	void SendPacket2CrossServer(uint32 opcode, const std::string_view& data);

	void SendPacket2CrossServer(const INetPacket &pck);
	void SendPacket2Mapserver(const INetPacket &pck);

	uint64 RPCInvoke2CrossServer(const INetPacket &pck,
		const std::function<void(INetStream&, int32, bool)> &cb = nullptr,
		AsyncTaskOwner *owner = nullptr, time_t timeout = DEF_S2S_RPC_TIMEOUT);
	uint64 RPCInvoke2Mapserver(const INetPacket &pck,
		const std::function<void(INetStream&, int32, bool)> &cb = nullptr,
		AsyncTaskOwner *owner = nullptr, time_t timeout = DEF_S2S_RPC_TIMEOUT);

	void RPCReply(const INetPacket &pck,
		uint64 sn, int32 err = RPCErrorNone, bool eof = true);

private:
	virtual void PushRPCPacket(const INetPacket &trans,
		const INetPacket &pck, const std::string_view &args);

	virtual RPCManager *GetRPCManager() {
		return &mgr_;
	}

	RPCManager mgr_;
};

#define sPacketDispatcher (*PacketDispatcher::instance())
