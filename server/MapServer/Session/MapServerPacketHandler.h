#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class MapServerService;

class MapServerPacketHandler :
	public MessageHandler<MapServerPacketHandler, MapServerService, MapServer2CrossServer::MAP2CROSS_MESSAGE_COUNT>,
	public Singleton<MapServerPacketHandler>
{
public:
	MapServerPacketHandler();
	virtual ~MapServerPacketHandler();
private:
	int HandleStartInstance(MapServerService *pService, INetPacket &pck);
	int HandleStopInstance(MapServerService *pService, INetPacket &pck);
	int HandleQueryPlayerLevelMax(MapServerService *pService, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
};

#define sMapServerPacketHandler (*MapServerPacketHandler::instance())
