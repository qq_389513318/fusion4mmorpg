#pragma once

#include "network/Session.h"

class GateServerSessionHandler;

class GateServerSession : public Session
{
public:
	GateServerSession();
	virtual ~GateServerSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnShutdownSession();

	void TransPacket(uint32 sn, const INetPacket& pck);
	void TransPacket(uint32 sn, const std::string_view& data);
	void TransPacket(uint32 sn,
		const INetPacket *pcks[], size_t pck_num,
		const std::string_view datas[], size_t data_num);

	void TransPacket2Instance(
		const INetPacket& pck, uint32 msSN, InstGUID instGuid);
	void TransPacket2Player(
		const INetPacket& pck, uint32 msSN, ObjGUID playerGuid);

	void TransPacket2Social4Player(
		uint32 uid, const INetPacket& pck);
	void TransPacket2Social4Player(
		const std::string_view& info, const INetPacket& pck);

	bool IsReady() const { return m_guid.serverId != 0; }
	const GateServerGUID& guid() const { return m_guid; }
	void resetGUID() { m_guid = {}; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void TransInstancePacket(INetPacket *pck) const;
	void TransPlayerPacket(INetPacket *pck) const;

	friend GateServerSessionHandler;
	GateServerGUID m_guid;
	uint32 m_gsIdx;
};
