#pragma once

#include "Singleton.h"
#include "ServerMaster.h"

class MapServerMaster : public IServerMaster, public Singleton<MapServerMaster>
{
public:
	MapServerMaster();
	virtual ~MapServerMaster();

	virtual bool InitSingleton();
	virtual void FinishSingleton();

private:
	virtual bool InitDBPool();
	virtual bool LoadDBData();

	virtual bool StartServices();
	virtual void StopServices();

	virtual void Tick();

	virtual std::string GetConfigFile();
	virtual size_t GetDeferAsyncServiceCount();
	virtual size_t GetAsyncServiceCount();
	virtual size_t GetIOServiceCount();

	bool CrossServer();
};

#define sMapServerMaster (*MapServerMaster::instance())
