#pragma once

#include "ServerService.h"

class CrossServerService : public IServerService
{
public:
	CrossServerService();
	virtual ~CrossServerService();

protected:
	virtual int HandlePacket(INetPacket& pck);
};
