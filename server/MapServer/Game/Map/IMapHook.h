#pragma once

#include "struct/teleport.h"

class MapInstance;

class IMapHook : public WheelTimerOwner, public WheelRoutineType
{
public:
	IMapHook(MapInstance* pMapInstance);
	virtual ~IMapHook();

	virtual void InitLuaEnv();

	virtual void InitArgs(const std::string& args) {}
	virtual void InitPlayArgs(const std::string_view& args) {}

	virtual void OnMapInstanceStart();

	virtual void Update(uint64 diffTime);

	virtual vector3f1f GetEntryPoint(
		uint32 gsIdx, const CharTeleportInfo& tpInfo) const;

	MapInstance* GetMapInstance() const { return m_pMapInstance; }
	const LuaRef& GetVariables() const { return m_variables; }

protected:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	void WatchShutdownMapInstance(
		uint32 tickMS = 5*1000, uint32 delayMS = 5*60*1000);
	void ShutdownMapInstance(uint32 delayMS = 0);

	MapInstance* const m_pMapInstance;
	LuaRef m_variables;
};
