#pragma once

#include "IMapHook.h"

class PlayBossHook : public IMapHook
{
public:
	PlayBossHook(MapInstance* pMapInstance);
	virtual ~PlayBossHook();

	virtual void InitLuaEnv();

	virtual void InitArgs(const std::string_view& args);

private:
	void SendPacket2Master(uint32 opcode, const std::string_view& data) const;

	static CreatureSpawnArgs NewCreatureSpawnArgs(const CreatureSpawn* pSpawn, uint32 level);
};
