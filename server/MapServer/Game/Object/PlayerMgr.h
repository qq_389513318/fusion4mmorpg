#pragma once

#include "Singleton.h"

class PlayerMgr : public Singleton<PlayerMgr>
{
public:
	PlayerMgr();
	virtual ~PlayerMgr();

	void AddPlayer(ObjGUID guid, InstGUID instGuid);
	void RemovePlayer(ObjGUID guid);

	InstGUID GetPlayerInstance(ObjGUID guid) const;

	size_t GetPlayerCount() const { return m_playerInstances.size(); }

private:
	mutable std::shared_mutex m_playerSharedMutex;
	std::unordered_map<ObjGUID, InstGUID> m_playerInstances;
};

#define sPlayerMgr (*PlayerMgr::instance())
