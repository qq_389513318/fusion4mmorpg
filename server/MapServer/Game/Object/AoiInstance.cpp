#include "preHeader.h"
#include "Creature.h"
#include "Player.h"
#include "StaticObject.h"
#include "AuraObject.h"

bool Player::TestInteresting(AoiActor *actor) const
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	return pLObj->IsVisibleByPlayer(this);
}

void Player::OnAddMarker(AoiActor *actor)
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	MaxNetPacket pack(SMSG_CREATE_OBJECT);
	pLObj->BuildCreatePacketForPlayer(pack, this);
	SendPacket(pack);
}

void Player::OnRemoveMarker(AoiActor *actor)
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	NetPacket pack(SMSG_REMOVE_OBJECT);
	pack << pLObj->GetGuid();
	SendPacket(pack);
}

bool Creature::TestInteresting(AoiActor *actor) const
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	if (pLObj->IsKindOf(TYPE_UNIT)) {
		auto pUnit = static_cast<Unit*>(pLObj);
		return IsHostile(pUnit);
	}
	return false;
}

void Creature::OnAddMarker(AoiActor *actor)
{
}

void Creature::OnRemoveMarker(AoiActor *actor)
{
}

bool StaticObject::TestInteresting(AoiActor *actor) const
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	if (pLObj->IsKindOf(TYPE_UNIT)) {
		if (m_pProto->sobjFlags.isPlayerInteresting &&
			pLObj->IsKindOf(TYPE_PLAYER)) {
			return true;
		}
		if (m_pProto->sobjFlags.isCreatureInteresting &&
			pLObj->IsKindOf(TYPE_CREATURE)) {
			return true;
		}
	}
	return false;
}

void StaticObject::OnAddMarker(AoiActor *actor)
{
	auto pUnit = static_cast<Unit*>(actor);
	ObjectHookEvent_OnSObjUnitEnter(pUnit);
}

void StaticObject::OnRemoveMarker(AoiActor *actor)
{
	auto pUnit = static_cast<Unit*>(actor);
	ObjectHookEvent_OnSObjUnitLeave(pUnit);
}

bool AuraObject::TestInteresting(AoiActor *actor) const
{
	auto pLObj = static_cast<LocatableObject*>(actor);
	if (pLObj->IsKindOf(TYPE_UNIT)) {
		auto pUnit = static_cast<Unit*>(pLObj);
		return CanApplyAuraEffect(pUnit);
	}
	return false;
}

void AuraObject::OnAddMarker(AoiActor *actor)
{
	auto pUnit = static_cast<Unit*>(actor);
	EnterAuraEffect(pUnit);
}

void AuraObject::OnRemoveMarker(AoiActor *actor)
{
	auto pUnit = static_cast<Unit*>(actor);
	LeaveAuraEffect(pUnit);
}
