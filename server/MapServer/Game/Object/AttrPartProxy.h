#pragma once

class Unit;
class Attribute;

class AttrPartProxy
{
public:
	AttrPartProxy(Unit* pOwner, ATTRPARTTYPE part);
	~AttrPartProxy();

	void Reset();

	void ApplyAttribute(uint32 type, double value);
	void ApplySpell(uint32 id, uint32 level);

private:
	Unit* const m_pOwner;
	ATTRPARTTYPE const m_part;

	struct SpellInfo {
		uint32 id;
		uint32 level;
	};
	std::vector<SpellInfo> m_availSpellInfos;
};
