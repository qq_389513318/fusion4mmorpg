#pragma once

class Unit;
class Player;
class StaticObject;
class LocatableObject;

class SpellEffects
{
public:
	static GErrorCode CanCast4Mine(Unit* pCaster,
		LocatableObject* pTarget, const SpellPrototype* pSpellProto,
		uint32 spellLevel, size_t effectIndex);
	static uint32 GetCastStageExtraTime4Mine(
		const Spell* pSpell, SpellStageType stage, size_t effectIndex);
	static void Start4Mine(Spell* pSpell, size_t effectIndex);
	static void Stop4Mine(Spell* pSpell, size_t effectIndex);
	static void Apply4Mine(
		Spell* pSpell, Player* pTarget, size_t effectIndex);
private:
	static StaticObject* GetMineTarget(const Spell* pSpell);
};
