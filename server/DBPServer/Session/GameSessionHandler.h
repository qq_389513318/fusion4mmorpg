#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/DBPProtocol.h"

class GameSession;

class GameSessionHandler :
	public MessageHandler<GameSessionHandler, GameSession, DBPProtocol::DBP_PROTOCOL_COUNT>,
	public Singleton<GameSessionHandler>
{
public:
	GameSessionHandler();
	virtual ~GameSessionHandler();
private:
	int HandleRegister(GameSession *pSession, INetPacket &pck);
	int HandleLoadAllInstCfgData(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveInstCfgData(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllPlayerCharacterInfo(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAccountPlayerPreviewInfo(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadOnePlayerCharacterInfo(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewOnePlayerInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveOnePlayerInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadOnePlayerInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteOnePlayerInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveMultiPlayerGsInfo(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleAcquireMailExpireTime(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleCleanExpireMail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGetPlayerMailCount(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGetPlayerMailList(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGetPlayerMailAttachment(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandlePlayerGetMailAttachment(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandlePlayerViewMailDetail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandlePlayerWriteMail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandlePlayerDeleteMail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSystemMultiSameMail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSystemMultiDifferentMail(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllGuildInfo(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllGuildMember(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllGuildApply(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewOneGuildInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveOneGuildInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteOneGuildInstance(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewOneGuildMember(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveOneGuildMember(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteOneGuildMember(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewOneGuildApply(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteGuildApply(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllSocialFriend(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllSocialIgnore(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteAllSocialData(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewSocialFriend(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteSocialFriend(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleNewSocialIgnore(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleDeleteSocialIgnore(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleLoadAllOperatingActivity(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleSaveOperatingActivity(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleCleanupOperatingActivity(GameSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
};

#define sGameSessionHandler (*GameSessionHandler::instance())
