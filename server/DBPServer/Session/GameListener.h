#pragma once

#include "Singleton.h"
#include "network/Listener.h"

class GameListener : public Listener, public Singleton<GameListener>
{
public:
	THREAD_RUNTIME(GameListener)

	GameListener();
	virtual ~GameListener();

private:
	virtual std::string GetBindAddress();
	virtual std::string GetBindPort();

	virtual Session *NewSessionObject();
};

#define sGameListener (*GameListener::instance())
