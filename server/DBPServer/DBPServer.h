#pragma once

#include "Singleton.h"
#include "FrameWorker.h"

class DBPServer : public FrameWorker, public AsyncTaskOwner,
	public Singleton<DBPServer>
{
public:
	THREAD_RUNTIME(DBPServer)

	DBPServer();
	virtual ~DBPServer();

private:
	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();
};

#define sDBPServer (*DBPServer::instance())
