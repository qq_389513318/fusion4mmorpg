package session

import (
	"test.com/fusion"
	"test.com/protocol"
)

var CenterServerSessionHandlers = [protocol.LOGIN2CENTER_MESSAGE_COUNT]func(*CenterServerSession, *fusion.NetPacket) int{
	protocol.SLC_REGISTER_RESP:           (*CenterServerSession).handleRegisterResp,
	protocol.SLC_PUSH_GAME_SERVER_LIST:   (*CenterServerSession).handlePushGameServerList,
	protocol.SLC_PUSH_GAME_SERVER_STATUS: (*CenterServerSession).handlePushGameServerStatus,
}

var CenterServerSessionRPCHandlers = [protocol.LOGIN2CENTER_MESSAGE_COUNT]func(*CenterServerSession, *fusion.NetPacket, *fusion.RPCReqMetaInfo) int{}
