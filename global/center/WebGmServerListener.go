package main

import (
	"log"
	"net/http"

	"test.com/center/session"
	"test.com/fusion"
)

func runWebGmServerListener() {
	var addr = cfg.WebGmServerListen
	log.Printf("start web gm listener `%s` goroutine successfully.\n", addr)

	mux := http.NewServeMux()
	mux.HandleFunc("/ReloadGameServerInfos.html", session.HandleReloadGameServerInfos)

	srv := &http.Server{Addr: addr, Handler: mux}
	fusion.AllWebServers.Store(srv, true)

	err := srv.ListenAndServe()
	if err != http.ErrServerClosed {
		log.Fatalf("listen web `%s` failed, %s.\n", addr, err)
	}
}
