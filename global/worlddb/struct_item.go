package worlddb

// ItemClass
const (
	ItemClass_None       = 0
	ItemClass_Equip      = 1 // 装备
	ItemClass_Quest      = 2 // 任务
	ItemClass_Material   = 3 // 材料
	ItemClass_Gemstone   = 4 // 宝石
	ItemClass_Consumable = 5 // 消耗品
	ItemClass_Score      = 6 // 积分
	ItemClass_Scrap      = 7 // 废品
	ItemClass_Other      = 8 // 其它
)

// ItemSubClass
const (
	ItemSubClass_Equip_Weapon                     = 1   // 武器
	ItemSubClass_Equip_Dress                      = 2   //衣服
	ItemSubClass_Equip_Helmet                     = 3   //头盔
	ItemSubClass_Equip_Necklace                   = 4   //项链
	ItemSubClass_Equip_Decoration                 = 5   //勋章
	ItemSubClass_Equip_Bracelet                   = 6   //手镯
	ItemSubClass_Equip_Ring                       = 7   //戒指
	ItemSubClass_Equip_Girdle                     = 8   //腰带
	ItemSubClass_Equip_Shoes                      = 9   //鞋子
	ItemSubClass_Equip_EquipDiamond               = 10  //魂玉
	ItemSubClass_Equip_SzBambooHat                = 11  //神装斗笠
	ItemSubClass_Equip_SztFaceNail                = 12  //神装面甲
	ItemSubClass_Equip_SzItemSubClass_Equip_Cape  = 13  //神装披风
	ItemSubClass_Equip_SzItemSubClass_Equip_Shiel = 14  //神装盾牌
	ItemSubClass_Equip_Pearl                      = 15  //宝珠
	ItemSubClass_Equip_MagicpWeapon               = 16  //魔器
	ItemSubClass_Equip_SilverNeedle               = 17  //银针
	ItemSubClass_Equip_Determination              = 18  //心决
	ItemSubClass_Equip_GoldSeal                   = 19  //金印
	ItemSubClass_Equip_SandersPearl               = 20  //檀珠
	ItemSubClass_Equip_SilverHairClasp            = 21  //银花簪
	ItemSubClass_Equip_HeartNourishingJade        = 22  //养心玉
	ItemSubClass_Equip_GoldObsidianBead           = 23  //金曜珠
	ItemSubClass_Equip_BlackSandalwoodStone       = 24  //黑檀石
	ItemSubClass_Equip_MzWeapon                   = 25  //魔剑
	ItemSubClass_Equip_MzDress                    = 26  //魔甲
	ItemSubClass_Equip_MzRing                     = 27  //魔戒
	ItemSubClass_Equip_MzBracelet                 = 28  //魔环
	ItemSubClass_Other_Items                      = 127 //普通物品
	ItemSubClass_Consumable_SlowHpdMed            = 128 //慢回金创药
	ItemSubClass_Consumable_SlowBluedMed          = 129 //慢回魔法药
	ItemSubClass_Consumable_FastMedicament        = 130 //瞬回药
	ItemSubClass_Consumable_Gift                  = 131 //礼包
	ItemSubClass_Consumable_RandomMove            = 132 //随机传送卷
	ItemSubClass_Consumable_DoubleExp             = 133 //多倍经验
)

// ItemQuality
const (
	ItemQuality_White = 0
	ItemQuality_Red   = 1
	ItemQuality_Count = 2
)

type ItemPrototype_Flags struct {
	CanUse            bool `json:"canUse,omitempty"`
	CanSell           bool `json:"canSell,omitempty"`
	CanDestroy        bool `json:"canDestroy,omitempty"`
	IsDestroyAfterUse bool `json:"isDestroyAfterUse,omitempty"`
}

type ItemPrototype struct {
	ItemTypeID   uint32              `json:"itemTypeID,omitempty" rule:"required"`
	ItemFlags    ItemPrototype_Flags `json:"itemFlags,omitempty" rule:"required"`
	ItemClass    uint32              `json:"itemClass,omitempty" rule:"required"`
	ItemSubClass uint32              `json:"itemSubClass,omitempty" rule:"required"`
	ItemQuality  uint32              `json:"itemQuality,omitempty" rule:"required"`
	ItemLevel    uint32              `json:"itemLevel,omitempty" rule:"required"`
	ItemStack    uint32              `json:"itemStack,omitempty" rule:"required"`

	ItemSellPrice uint32 `json:"itemSellPrice,omitempty" rule:"required"`

	ItemLootId     uint32   `json:"itemLootId,omitempty" rule:"required"`
	ItemSpellId    uint32   `json:"itemSpellId,omitempty" rule:"required"`
	ItemSpellLevel uint32   `json:"itemSpellLevel,omitempty" rule:"required"`
	ItemScriptId   uint32   `json:"itemScriptId,omitempty" rule:"required"`
	ItemScriptArgs string   `json:"itemScriptArgs,omitempty" rule:"required"`
	ItemParams     []uint32 `json:"itemParams,omitempty" rule:"required"`
}

func (*ItemPrototype) GetTableName() string {
	return "item_prototype"
}
func (*ItemPrototype) GetTableKeyName() string {
	return "itemTypeID"
}
func (obj *ItemPrototype) GetTableKeyValue() uint {
	return uint(obj.ItemTypeID)
}

type ItemEquipPrototype_Attr struct {
	Type  uint32  `json:"type,omitempty"`
	Value float64 `json:"value,omitempty"`
}

type ItemEquipPrototype_Spell struct {
	Id    uint32 `json:"id,omitempty"`
	Level uint32 `json:"level,omitempty"`
}

type ItemEquipPrototype struct {
	ItemTypeID uint32 `json:"itemTypeID,omitempty" rule:"required"`

	ItemEquipAttrs []ItemEquipPrototype_Attr `json:"itemEquipAttrs,omitempty" rule:"required"`

	ItemEquipSpells []ItemEquipPrototype_Spell `json:"itemEquipSpells,omitempty" rule:"required"`
}

func (*ItemEquipPrototype) GetTableName() string {
	return "item_equip_prototype"
}
func (*ItemEquipPrototype) GetTableKeyName() string {
	return "itemTypeID"
}
func (obj *ItemEquipPrototype) GetTableKeyValue() uint {
	return uint(obj.ItemTypeID)
}
