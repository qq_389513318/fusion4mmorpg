package worlddb

// CharRaceType
const (
	CharRaceType_Player         = 0
	CharRaceType_Neutral        = 1
	CharRaceType_HostileForces  = 2
	CharRaceType_FriendlyForces = 3
)

// CharEliteType
const (
	CharEliteType_Normal = 0
	CharEliteType_Boss   = 1
)

type CharPrototype_Flags struct {
	IsUndead          bool `json:"isUndead,omitempty"`
	IsJoinCombat      bool `json:"isJoinCombat,omitempty"`
	IsFastTurn        bool `json:"isFastTurn,omitempty"`
	IsKeepDir         bool `json:"isKeepDir,omitempty"`
	IsActivity        bool `json:"isActivity,omitempty"`
	IsIgnoreInvisible bool `json:"isIgnoreInvisible,omitempty"`
	IsSenseCreature   bool `json:"isSenseCreature,omitempty"`
	IsSensePlayer     bool `json:"isSensePlayer,omitempty"`
}

type CharPrototype struct {
	CharTypeId uint32              `json:"charTypeId,omitempty" rule:"required"`
	CharFlags  CharPrototype_Flags `json:"charFlags,omitempty" rule:"required"`
	CharRace   uint32              `json:"charRace,omitempty" rule:"required"`
	CharElite  uint32              `json:"charElite,omitempty" rule:"required"`
	AttrType   uint32              `json:"attrType,omitempty" rule:"required"`
	Level      uint32              `json:"level,omitempty" rule:"required"`

	SpeedWalk float32 `json:"speedWalk,omitempty" rule:"required"`
	SpeedRun  float32 `json:"speedRun,omitempty" rule:"required"`
	SpeedTurn float32 `json:"speedTurn,omitempty" rule:"required"`

	PatrolRange  float32 `json:"patrolRange,omitempty" rule:"required"`
	BoundRadius  float32 `json:"boundRadius,omitempty" rule:"required"`
	SenseRadius  float32 `json:"senseRadius,omitempty" rule:"required"`
	CombatRadius float32 `json:"combatRadius,omitempty" rule:"required"`

	CombatBestDist float32 `json:"combatBestDist,omitempty" rule:"required"`

	MinRespawnTime    uint32 `json:"minRespawnTime,omitempty" rule:"required"`
	MaxRespawnTime    uint32 `json:"maxRespawnTime,omitempty" rule:"required"`
	DeadDisappearTime uint32 `json:"deadDisappearTime,omitempty" rule:"required"`

	RecoveryHPRate  float64 `json:"recoveryHPRate,omitempty" rule:"required"`
	RecoveryHPValue float64 `json:"recoveryHPValue,omitempty" rule:"required"`
	RecoveryMPRate  float64 `json:"recoveryMPRate,omitempty" rule:"required"`
	RecoveryMPValue float64 `json:"recoveryMPValue,omitempty" rule:"required"`

	LootSetID uint32 `json:"lootSetID,omitempty" rule:"required"`

	AiScriptId      uint32 `json:"aiScriptId,omitempty" rule:"required"`
	SpawnScriptId   uint32 `json:"spawnScriptId,omitempty" rule:"required"`
	SpawnScriptArgs string `json:"spawnScriptArgs,omitempty" rule:"required"`
	DeadScriptId    uint32 `json:"deadScriptId,omitempty" rule:"required"`
	DeadScriptArgs  string `json:"deadScriptArgs,omitempty" rule:"required"`
	PlayScriptId    uint32 `json:"playScriptId,omitempty" rule:"required"`
	PlayScriptArgs  string `json:"playScriptArgs,omitempty" rule:"required"`
}

func (*CharPrototype) GetTableName() string {
	return "char_prototype"
}
func (*CharPrototype) GetTableKeyName() string {
	return "charTypeId"
}
func (obj *CharPrototype) GetTableKeyValue() uint {
	return uint(obj.CharTypeId)
}

type CreatureSpawn_Flags struct {
	IsRespawn     bool `json:"isRespawn,omitempty"`
	IsPlaceholder bool `json:"isPlaceholder,omitempty"`
}

type CreatureSpawn struct {
	SpawnId    uint32              `json:"spawnId,omitempty" rule:"required"`
	Flags      CreatureSpawn_Flags `json:"flags,omitempty" rule:"required"`
	Entry      uint32              `json:"entry,omitempty" rule:"required"`
	Level      uint32              `json:"level,omitempty" rule:"required"`
	Round      uint32              `json:"round,omitempty" rule:"required"`
	Map_id     uint32              `json:"map_id,omitempty" rule:"required"`
	Map_type   uint32              `json:"map_type,omitempty" rule:"required"`
	X          float32             `json:"x,omitempty" rule:"required"`
	Y          float32             `json:"y,omitempty" rule:"required"`
	Z          float32             `json:"z,omitempty" rule:"required"`
	O          float32             `json:"o,omitempty" rule:"required"`
	IdleType   uint32              `json:"idleType,omitempty" rule:"required"`
	WayPointId uint32              `json:"wayPointId,omitempty" rule:"required"`
}

func (*CreatureSpawn) GetTableName() string {
	return "creature_spawn"
}
func (*CreatureSpawn) GetTableKeyName() string {
	return "spawnId"
}
func (obj *CreatureSpawn) GetTableKeyValue() uint {
	return uint(obj.SpawnId)
}
