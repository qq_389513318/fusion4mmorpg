package worlddb

// QuestNavIdx
const (
	QuestNavIdx_Publisher = -1
	QuestNavIdx_Verifier  = -2
	QuestNavIdx_Guider    = -3
)

// QuestClassType
const (
	QuestClassType_None       = 0
	QuestClassType_MainLine   = 1
	QuestClassType_BranchLine = 2
)

// QuestRepeatType
const (
	QuestRepeatType_None    = 0
	QuestRepeatType_Daily   = 1
	QuestRepeatType_Weekly  = 2
	QuestRepeatType_Monthly = 3
	QuestRepeatType_Forever = 4
	QuestRepeatType_Count   = 5
)

// QuestWhenType
const (
	QuestWhenType_Accept = 0
	QuestWhenType_Finish = 1
	QuestWhenType_Failed = 2
	QuestWhenType_Cancel = 3
	QuestWhenType_Submit = 4
	QuestWhenType_Count  = 5
)

// QuestObjType
const (
	QuestObjType_None   = 0
	QuestObjType_Guide  = 1
	QuestObjType_NPC    = 2
	QuestObjType_SObj   = 3
	QuestObjType_NPCPt  = 4
	QuestObjType_SObjPt = 5
	QuestObjType_Count  = 6
)

type QuestObjInst struct {
	ObjType uint32 `json:"objType,omitempty"`
	ObjID   uint32 `json:"objID,omitempty"`
}

type QuestScript struct {
	ScriptID   uint32 `json:"scriptID,omitempty"`
	ScriptArgs string `json:"scriptArgs,omitempty"`
}

type QuestCheque struct {
	ChequeType  uint8  `json:"chequeType,omitempty"`
	ChequeValue uint64 `json:"chequeValue,omitempty"`
}

type QuestItem struct {
	ItemTypeID uint32 `json:"itemTypeID,omitempty"`
	ItemCount  uint32 `json:"itemCount,omitempty"`
	OnlyCareer uint32 `json:"onlyCareer,omitempty"`
	OnlyGender uint32 `json:"onlyGender,omitempty"`
}

type QuestChequeReq struct {
	QuestCheque

	IsCost   bool `json:"isCost,omitempty"`
	IsRefund bool `json:"isRefund,omitempty"`
}

type QuestItemReq struct {
	QuestItem

	IsDestroy bool `json:"isDestroy,omitempty"`
	IsRefund  bool `json:"isRefund,omitempty"`
	IsBinding bool `json:"isBinding,omitempty"`
}

type QuestQuestionReq struct {
	QuestionId uint32 `json:"questionId,omitempty"`
	OptionId   uint8  `json:"optionId,omitempty"`
}

type QuestChequeInit struct {
	QuestCheque

	IsRetrieve bool `json:"isRetrieve,omitempty"`
}

type QuestItemInit struct {
	QuestItem

	IsBinding  bool `json:"isBinding,omitempty"`
	IsRetrieve bool `json:"isRetrieve,omitempty"`
}

type QuestChequeReward struct {
	QuestCheque

	IsFixed bool `json:"isFixed,omitempty"`
}

type QuestItemReward struct {
	QuestItem

	IsBinding bool `json:"isBinding,omitempty"`
}

// QuestConditionType
const (
	QuestConditionType_None         = 0
	QuestConditionType_TalkNPC      = 1
	QuestConditionType_KillCreature = 2
	QuestConditionType_HaveCheque   = 3
	QuestConditionType_HaveItem     = 4
	QuestConditionType_UseItem      = 5
	QuestConditionType_PlayStory    = 6
	QuestConditionType_Count        = 7
)

type QuestCondition struct {
	ConditionType  uint32       `json:"conditionType,omitempty"`
	ConditionIds   []uint32     `json:"conditionIds,omitempty"`
	ConditionNum   uint64       `json:"conditionNum,omitempty"`
	ConditionArgs  []uint32     `json:"conditionArgs,omitempty"`
	ConditionFlags []bool       `json:"conditionFlags,omitempty"`
	ForNavInfo     QuestObjInst `json:"forNavInfo,omitempty"`
}

type QuestPrototype_Flags struct {
	IsWatchStatus    bool `json:"isWatchStatus,omitempty"`
	IsAutoAccept     bool `json:"isAutoAccept,omitempty"`
	IsAutoSubmit     bool `json:"isAutoSubmit,omitempty"`
	IsStoryMode      bool `json:"isStoryMode,omitempty"`
	IsEnterSceneAeap bool `json:"isEnterSceneAeap,omitempty"`
	IsLeaveSceneAeap bool `json:"isLeaveSceneAeap,omitempty"`
	IsCantArchive    bool `json:"isCantArchive,omitempty"`
	IsCantCancel     bool `json:"isCantCancel,omitempty"`
	IsAnyReqQuest    bool `json:"isAnyReqQuest,omitempty"`
	IsAnyCondition   bool `json:"isAnyCondition,omitempty"`
	IsRemoveFailed   bool `json:"isRemoveFailed,omitempty"`
}

type QuestPrototype struct {
	QuestTypeID    uint32               `json:"questTypeID,omitempty" rule:"required"`
	QuestClass     uint32               `json:"questClass,omitempty" rule:"required"`
	QuestFlags     QuestPrototype_Flags `json:"questFlags,omitempty" rule:"required"`
	QuestPublisher QuestObjInst         `json:"questPublisher,omitempty" rule:"required"`
	QuestVerifier  QuestObjInst         `json:"questVerifier,omitempty" rule:"required"`
	QuestGuider    QuestObjInst         `json:"questGuider,omitempty" rule:"required"`
	QuestTimeMax   uint32               `json:"questTimeMax,omitempty" rule:"required"`

	QuestRepeatType uint32 `json:"questRepeatType,omitempty" rule:"required"`
	QuestRepeatMax  uint32 `json:"questRepeatMax,omitempty" rule:"required"`

	QuestReqMinLv    uint32           `json:"questReqMinLv,omitempty" rule:"required"`
	QuestReqMaxLv    uint32           `json:"questReqMaxLv,omitempty" rule:"required"`
	QuestReqCareer   uint32           `json:"questReqCareer,omitempty" rule:"required"`
	QuestReqGender   uint32           `json:"questReqGender,omitempty" rule:"required"`
	QuestReqQuests   []uint32         `json:"questReqQuests,omitempty" rule:"required"`
	QuestReqCheques  []QuestChequeReq `json:"questReqCheques,omitempty" rule:"required"`
	QuestReqItems    []QuestItemReq   `json:"questReqItems,omitempty" rule:"required"`
	QuestReqQuestion QuestQuestionReq `json:"questReqQuestion,omitempty" rule:"required"`
	QuestReqExtra    QuestScript      `json:"questReqExtra,omitempty" rule:"required"`

	QuestInitCheques []QuestChequeInit `json:"questInitCheques,omitempty" rule:"required"`
	QuestInitItems   []QuestItemInit   `json:"questInitItems,omitempty" rule:"required"`
	QuestInitExtra   QuestScript       `json:"questInitExtra,omitempty" rule:"required"`

	QuestRewardCheques []QuestChequeReward `json:"questRewardCheques,omitempty" rule:"required"`
	QuestRewardItems   []QuestItemReward   `json:"questRewardItems,omitempty" rule:"required"`
	QuestRewardExtra   QuestScript         `json:"questRewardExtra,omitempty" rule:"required"`

	QuestScripts    []QuestScript    `json:"questScripts,omitempty" rule:"required"`
	QuestConditions []QuestCondition `json:"questConditions,omitempty" rule:"required"`
}

func (*QuestPrototype) GetTableName() string {
	return "quest_prototype"
}
func (*QuestPrototype) GetTableKeyName() string {
	return "questTypeID"
}
func (obj *QuestPrototype) GetTableKeyValue() uint {
	return uint(obj.QuestTypeID)
}

type QuestCreatureVisible struct {
	Id            uint64   `json:"Id,omitempty" rule:"required"`
	QuestTypeID   uint32   `json:"questTypeID,omitempty" rule:"required"`
	QuestWhenType int8     `json:"questWhenType,omitempty" rule:"required"`
	IsVisible     bool     `json:"isVisible,omitempty" rule:"required"`
	SpawnIDs      []uint32 `json:"spawnIDs,omitempty" rule:"required"`
}

func (*QuestCreatureVisible) GetTableName() string {
	return "quest_creature_visible"
}
func (*QuestCreatureVisible) GetTableKeyName() string {
	return "Id"
}
func (obj *QuestCreatureVisible) GetTableKeyValue() uint {
	return uint(obj.Id)
}
