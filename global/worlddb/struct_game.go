package worlddb

// GameTime_Type
const (
	GameTime_Type_None        = 0
	GameTime_Type_PlayBoss    = 1
	GameTime_Type_GuildLeague = 2
)

type GameTime struct {
	Id      uint32 `json:"Id,omitempty" rule:"required"`
	StrTime string `json:"strTime,omitempty" rule:"required"`
	StrDesc string `json:"strDesc,omitempty" rule:"required"`
}

func (*GameTime) GetTableName() string {
	return "game_time"
}
func (*GameTime) GetTableKeyName() string {
	return "Id"
}
func (obj *GameTime) GetTableKeyValue() uint {
	return uint(obj.Id)
}
