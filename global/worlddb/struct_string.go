package worlddb

// STRING_TEXT_TYPE
const (
	STRING_TEXT_TYPE_SYS_MSG         = 0
	STRING_TEXT_TYPE_CLIENT_TEXT     = 1
	STRING_TEXT_TYPE_ERROR_TEXT      = 2
	STRING_TEXT_TYPE_MAP_NAME        = 3
	STRING_TEXT_TYPE_ZONE_NAME       = 4
	STRING_TEXT_TYPE_CHAR_NAME       = 5
	STRING_TEXT_TYPE_CHAR_DESC       = 6
	STRING_TEXT_TYPE_SOBJ_NAME       = 7
	STRING_TEXT_TYPE_SOBJ_DESC       = 8
	STRING_TEXT_TYPE_QUEST_NAME      = 9
	STRING_TEXT_TYPE_QUEST_DESC      = 10
	STRING_TEXT_TYPE_QUEST_INTRO     = 11
	STRING_TEXT_TYPE_QUEST_DONE      = 12
	STRING_TEXT_TYPE_QUEST_NOT_DONE  = 13
	STRING_TEXT_TYPE_QUEST_CUSTOMIZE = 14
	STRING_TEXT_TYPE_ITEM_NAME       = 15
	STRING_TEXT_TYPE_ITEM_DESC       = 16
	STRING_TEXT_TYPE_SPELL_NAME      = 17
	STRING_TEXT_TYPE_SPELL_DESC      = 18
	STRING_TEXT_TYPE_BUFF_DESC       = 19
	STRING_TEXT_TYPE_QUESTION        = 20
)

type String_text_list struct {
	StringID uint32 `json:"stringID,omitempty" rule:"required"`
	StringEN string `json:"stringEN,omitempty" rule:"required"`
	StringCN string `json:"stringCN,omitempty" rule:"required"`
}

func (*String_text_list) GetTableName() string {
	return "string_text_list"
}
func (*String_text_list) GetTableKeyName() string {
	return "stringID"
}
func (obj *String_text_list) GetTableKeyValue() uint {
	return uint(obj.StringID)
}
