package main

import (
	"test.com/worlddb"
)

func getEnumScriptType() map[string]int {
	return map[string]int{
		"None":         worlddb.ScriptType_None,
		"AIGraphml":    worlddb.ScriptType_AIGraphml,
		"CharSpawn":    worlddb.ScriptType_CharSpawn,
		"CharDead":     worlddb.ScriptType_CharDead,
		"CharPlay":     worlddb.ScriptType_CharPlay,
		"SObjSpawn":    worlddb.ScriptType_SObjSpawn,
		"SObjPlay":     worlddb.ScriptType_SObjPlay,
		"QuestReq":     worlddb.ScriptType_QuestReq,
		"QuestInit":    worlddb.ScriptType_QuestInit,
		"QuestReward":  worlddb.ScriptType_QuestReward,
		"QuestEvent":   worlddb.ScriptType_QuestEvent,
		"CanCastSpell": worlddb.ScriptType_CanCastSpell,
		"UseItem":      worlddb.ScriptType_UseItem,
	}
}

func getEnumSpellEffectType() map[string]int {
	return map[string]int{
		"None":        worlddb.SpellEffectType_None,
		"Attack":      worlddb.SpellEffectType_Attack,
		"AuraAttack":  worlddb.SpellEffectType_AuraAttack,
		"ChangeProp":  worlddb.SpellEffectType_ChangeProp,
		"ChangeSpeed": worlddb.SpellEffectType_ChangeSpeed,
		"Invincible":  worlddb.SpellEffectType_Invincible,
		"Invisible":   worlddb.SpellEffectType_Invisible,
		"Fetter":      worlddb.SpellEffectType_Fetter,
		"Stun":        worlddb.SpellEffectType_Stun,
		"AuraObject":  worlddb.SpellEffectType_AuraObject,
		"Teleport":    worlddb.SpellEffectType_Teleport,
		"Mine":        worlddb.SpellEffectType_Mine,
	}
}

func getEnumQuestObjType() map[string]int {
	return map[string]int{
		"None":   worlddb.QuestObjType_None,
		"Guide":  worlddb.QuestObjType_Guide,
		"NPC":    worlddb.QuestObjType_NPC,
		"SObj":   worlddb.QuestObjType_SObj,
		"NPCPt":  worlddb.QuestObjType_NPCPt,
		"SObjPt": worlddb.QuestObjType_SObjPt,
	}
}

func getEnumQuestWhenType() map[string]int {
	return map[string]int{
		"Accept": worlddb.QuestWhenType_Accept,
		"Finish": worlddb.QuestWhenType_Finish,
		"Failed": worlddb.QuestWhenType_Failed,
		"Cancel": worlddb.QuestWhenType_Cancel,
		"Submit": worlddb.QuestWhenType_Submit,
		"Count":  worlddb.QuestWhenType_Count,
	}
}

func getEnumQuestConditionType() map[string]int {
	return map[string]int{
		"None":            worlddb.QuestConditionType_None,
		"TalkNPC":         worlddb.QuestConditionType_TalkNPC,
		"KillCreature":    worlddb.QuestConditionType_KillCreature,
		"HaveCheque":      worlddb.QuestConditionType_HaveCheque,
		"HaveItem":        worlddb.QuestConditionType_HaveItem,
		"UseItem":         worlddb.QuestConditionType_UseItem,
		"PlayStory":       worlddb.QuestConditionType_PlayStory,
	}
}
