# fusion4mmorpg

#### 项目介绍
[fusion](https://gitee.com/jallyx/fusion)是MMORPG项目的一个稳定、高效、简单易用、功能强大的底层库实现。<br>
用谦虚的话来说，其实就是一个编写游戏服务器的底层功能库集合，我把所有可以被公用的功能模块全部集成到了这个库里面，基本上可以说，只要是你需要的基础功能，这个库里面应该都有一份对应的功能实现。<br>
本项目原本是作为fusion库的使用示范代码存在的，也是我多年来总结的服务器编程经验，最终终于达到了基本可用状态。到目前为此，除去需要策划参与的数值设计，玩法设计，本项目已经完成了一个MMORPG服务器所有模块的功能实现，并对基本玩法测试到了无Bug状态，如地图跳转、玩家上线、下线逻辑、技能、打怪、做任务、怪物掉落、采集、怪物基础AI、场景AOI等。<br>
在做项目的时候，有时候我会想，如果这个模块能够推倒重来，那么我应该怎么去设计它呢？公司的代码无法轻易改动设计，但是自己的项目却完全可以按照自己的最优标准来设计。当实际的项目缺少某个模块实现时，我也可以快速移植过去。这或许算是这个项目的由来吧。<br>
本项目采用C++做服务器主框架，Lua编写玩法逻辑。在主框架中，对可能需要关注或修改逻辑的流程中，预先埋入Hook，在实际的玩法逻辑编写中，可以根据需要，注入自己的私有逻辑到原流程中，从而实现各种上层玩法逻辑。<br>
本项目设计的理论承载上限是单服1万玩家。<br>

#### 软件架构
![基础架构图](https://gitee.com/jallyx/fusion4mmorpg/raw/master/doc/assets/a1.jpg "基础架构")<br>
核心服务器<br>
GateServer: 网关服务器，负责接收来自客户端的连接，然后验证玩家的账号Token，当账号验证通过后，将来自客户端的数据包转发给对应的内部服务进程，将内部服务进程需要发送给玩家的数据包转发给客户端；如果通信协议数据需要加密，本服务器也将负责这部分工作。<br>
GameServer: 中心调度服务器，负责玩家上线、下线逻辑，玩家在地图之间跳转的逻辑，副本动态创建、销毁的逻辑；组队、好友、聊天、邮件发送功能；当需要实现全服相关的逻辑时，功能通常需要在本服务器实现，比如多人报名参与活动、商品限量抢购等。<br>
DBPServer: 负责本服玩家数据、服务器实例数据的写入与读取，并对频繁读取的数据实现缓存功能。<br>
SocialServer: 负责社交系统功能，工会、全服排行榜。<br>
MapServer: 地图服务器，游戏服务器的核心，AOI、AI、任务、背包、战斗、技能、掉落、数值成长线等各种与玩家相关的功能都由本服务器负责实现；另外一点，按照目前设计，当涉及到跨服玩法时，本服务器可能也会负责玩法管理的功能。<br>
非核心服务器<br>
LoginServer: 在玩家连接服务器之前，负责提供SDK账号验证，验证通过后，拉取服务器列表，账号下所有角色列表等功能。<br>
CenterServer：负责验证玩家进入游戏服务器的账号Token；GM管理模块；整个系统唯一数据的生成，如玩家GUID、名称等。<br>
客户端与服务器的交互流程<br>
客户端首先需要通过http协议与LoginServer交互，验证玩家账号、密码是否正确，如果验证通过，则下发账号Token给客户端，接下来客户端可以利用该账号标识来拉取所有的游戏服务器列表、该账号下所有的角色列表。<br>
玩家可以从游戏服务器列表中选择一个游戏服务器，也可以根据角色所在的游戏服务器选择一个游戏服务器，通过tcp协议连接到GateServer，开始游戏流程。<br>
GateServer首先必须要验证玩家的合法性，客户端把LoginServer生成的账号Token发送给GateServer，GateServer会把玩家的账号Token转发给CenterServer验证，如果验证通过，则通知GameServer生成一个Account对象，代表一个有效的账号连接。LoginServer验证账号通过后，同时也会把账号Token写入Redis，CenterServer需要验证账号Token时，就会从Redis中读取此数据并进行比对。<br>
GameServer生成了Account对象后，客户端就可以执行拉取角色列表、创建角色、删除角色，或选择一个角色登陆到MapServer游戏。<br>

![扩展架构图](https://gitee.com/jallyx/fusion4mmorpg/raw/master/doc/assets/a2.jpg "扩展架构")<br>
上述架构图展示了，一组服务器支持开启多个GateServer进行负载均衡，也支持将地图场景分组，然后开启多个MapServer，每个MapServer负责其中一组地图场景的逻辑。<br>

![跨服架构图](https://gitee.com/jallyx/fusion4mmorpg/raw/master/doc/assets/a3.jpg "跨服架构")<br>
上述架构图展示了，当一个MapServer同时向两个GameServer注册时，如果这两个GameServer指派了完全相同的地图场景要求此MapServer服务，那么来自于这两组服务器的玩家进入到了这些场景后，彼此之间就完全可见、可以进行任何交互，就如同所有的玩家都是本服玩家一样。<br>
在服务器的所有代码实现中，基本上找不到为跨服功能专门编写的代码逻辑，然而采用此架构设计，跨服功能已经完全运行起来了。<br>

个人看法，在目前本项目的实现中，有三个模块设计是我最引以为傲的，首先的就是fusion这个底层库，它算是反复改进、优化的一个结果吧；其次是上层逻辑Hook系统的设计；最后就是这个跨服设计了。

#### 安装教程

python3.7.0(或更高版本) <br>
`py -3 -m pip install ply` <br>
`py -3 -m pip install pymysql` <br>
`py -3 -m pip install openpyxl` <br>
以上命令用于安装python第三方模块，项目内部的相关工具会使用到。<br>

go1.13.0(或更高版本) <br>
`set GO111MODULE=on` <br>
`set GOPROXY=https://goproxy.io` <br>
以上环境变量只需在第一次执行`go install ...`时使用即可。<br>
`cd fusion4mmorpg/global` # 进入到go工程目录，这儿写的是相对于整个项目的路径。<br>
`go install test.com/editor` # 生成服务器数据配置编辑器 <br>
`go install test.com/center` # CenterServer <br>
`go install test.com/login` # LoginServer <br>
以上执行文件会被放到GOPATH/bin目录下，需要开发者自行移动到`fusion4mmorpg/global/bin`目录，这些文件的工作目录必须是这里。<br>
`cd fusion4mmorpg/global/tools; go install db2file.go;` # 导出游戏配置数据供客户端使用，目前客户端测试项目是[kbengine_unity3d_demo](https://gitee.com/jallyx/kbengine_unity3d_demo)。

vs2017 <br>
`fusion4mmorpg/server/thirdparty/boost.zip` # 需要把这个压缩包解压到其所在目录，boost库太大了，上传压缩包快得多。<br>
`fusion4mmorpg/server/Project/win/MMORPG.sln` # vs的解决方案，打开后直接编译即可，win平台只有debug版本。<br>
`fusion4mmorpg/middata/deploy4server.bat` # 把服务器需要的资源文件拷贝到服务器的工作目录。<br>
`fusion4mmorpg/server/Project/build/debug/CopyEnv.bat` # 把服务器需要的所有依赖文件拷贝到服务器的工作目录，并适当调整配置文件。这个批处理里面涉及参数配置，需要自行根据实际情况改动，一般来说，简单的把目标ip地址改成`127.0.0.1`即可。<br>

mysql8.0(或更高版本) <br>
`app 123456` # 读取、写入服务器数据的账号 <br>
`editor 123456` # 编辑器读取、写入的账号 <br>
`viewer 123456` # 服务器读取配置数据的账号 <br>
以上三个账号请自行创建，可以简单的赋予所有权限。<br>
`fusion4mmorpg/server/Project/build/mysql` # 数据库的表定义SQL，以及测试数据。<br>
`INSERT INTO mmorpg_global.t_game_servers VALUES(NULL, '127.0.0.1', 9999, '127.0.0.1', 'main', 0, '', 0, 0);` # 游戏服务器必须被CenterServer注册后才能提供服务，这儿给游戏服务器添加一条注册数据，否则游戏服务器会启动失败。<br>

`fusion4mmorpg/global/bin` # 执行`start.bat` <br>
`fusion4mmorpg/server/Project/build/debug` # 执行`start.bat` <br>
至此，整个游戏服务器算是启动成功了。<br>

#### 使用说明

请自行下载[kbengine_unity3d_demo](https://gitee.com/jallyx/kbengine_unity3d_demo)这个客户端项目进行测试。<br>
unity2018.3.6 <br>
本项目是以`kbengine_unity3d_demo`这个项目为底子魔改而来，目前跟`kbengine_unity3d_demo`已经没有啥关系了，内部使用的全是当前项目需要的东西。至于为啥会这样做嘛？那是因为我对unity基本上是一窍不通，但是我又需要一个客户端来验证服务器功能，所以只好找一个现成的修改了。<br>

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)