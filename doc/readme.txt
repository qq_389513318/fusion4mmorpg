1. 地图类型支持
一种地图类型实现一种玩法，玩法的逻辑采用IMapHook实现。(当然你也可以在IMapHook中实现多种玩法，不过这不是我所推荐的做法。)
如果一个地图实例上需要实现多种玩法，则需要启用MapHookInfo的支持。一个地图实例上可以挂载多个MapHookInfo实例，它们之间可以独立运行，也可以相互交互运行。
在目前的示例实现中，我采用的是IMapHook与MapHookInfo配合实现特殊玩法。

2. 设计目标
0宕机，至少进程不能崩溃。
方便查找错误，对数据的修改应该尽量保持单一入口。
高度抽象的框架，框架应当只完成基础功能。框架需要提供必要的玩法接口，这些接口需要保持功能单一，然后上层的逻辑玩法全部由脚本完成，由脚本自己决定应该触发哪些接口，而不是用一个函数去处理所有可能存在的逻辑情况，产生大量if分支。
无缝跨服，不仅仅是玩家的无缝体验，对于策划、编程人员也应做到无缝体验，尽量不要因为跨服而特殊处理代码。
